<?php
include "includes/config_vendor.php";

session_start();
if(!isset($_SESSION['username'])){
    echo "<script>window.alert('You Must Be Log In !')
    window.location='./login.php'</script>";
}


$id = $_SESSION['id'];
$terms = array('_id' => $id);
$response = $collection->findOne($terms);



?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/Logo.png"> 
    <title>Edit Profile</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="css/editprofil.css">

    <style type="text/css">
    #img-upload{
        border: 1px solid black;
        padding: 10px;
        margin-top: 10px;
    }
    #img-upload img{
        border: 1px solid black;
        width: 150px;
        height: 150px;
        padding: 5px;
    }
    #preview{
        display: inline;
        padding: 10px;
    }
    #preview img{
        border: 1px solid black;
        width: 150px;
        height: 150px;
        padding: 5px;
        margin-bottom: 20px;
    }
</style>
</head>


<body>
 <?php
 include 'navbar.php';
 ?>
 <div class="container">
   <div class="row">
    <div class="text-h1">
        <p class="text-p">Edit Profil</p>
    </div>
</div>
<form action="api/path/updateprofile.php" method="post" enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-md-offset-1 col-md-push-4">
                <?php
                if ($response['picture'] != null){
                    echo '<div id="preview"><a href="editphotoprofil.php?id='.$_SESSION['id'].'&username='.$response['uname'].'">
                    <img style="width:150px;height:150px;" alt="" src="data:image/jpeg;base64,'.$response['picture'].'">  </div>
                    <p style="margin-left:25px;">Change Photo Profile</p></a>';
                } else {
                    echo '<div id="preview"><a href="editphotoprofil.php?id='.$_SESSION['id'].'&username='.$response['uname'].'">
                    <img style="width:150px;height:150px;" alt="" src="assets/img/image.jpg"> </div>
                    <p style="margin-left:25px;">Change Photo Profile</p> </a>';
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-push-4">
                <p class="text-left">Name </p>
            </div>
            <div class="col-md-3 col-md-push-4">
                <input class="form-control" type="text" required="" name="name" value="<?php echo $response['name'];?>" >
                <input class="form-control" type="text" required="" name="id" value="<?php echo $_SESSION['id'];?>" style="display: none;">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-push-4">
                <p class="text-left">Birthday </p>
            </div>
            <div class="col-md-3 col-md-push-4">
                <input class="form-control" type="date" required="" name="birthday_date" value="<?php echo $response['birthday_date'];?>" >
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-push-4">
                <p class="text-left">Vendor Name </p>
            </div>
            <div class="col-md-3 col-md-push-4">
                <input class="form-control" type="text" required="" name="vname" value="<?php echo $response['vname'];?>" >
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-push-4">
                <p class="text-left">No Handphone </p>
            </div>
            <div class="col-md-3 col-md-push-4">
                <input class="form-control" type="text" required="" name="no_hp" value="<?php echo $response['no_hp'];?>" onkeydown="return numbersonly(this, event);">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-push-4">
                <p class="text-left">Vendor Address </p>
            </div>
            <div class="col-md-3 col-md-push-4">
                <textarea class="form-control" required="" rows="1" cols="50" name="vaddress"><?php echo $response['vaddress'];?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-push-4">
                <p class="text-left">Provinsi </p>
            </div>
            <div class="col-md-3 col-md-push-4">
                <select class="form-control input-sm" name="provinsi">
                    <optgroup label="Jawa Barat">
                        <option value="Kabupaten Bandung" <?php if (!empty($response['provinsi']) && $response['provinsi'] == 'Kabupaten Bandung')  echo 'selected = "selected"'; ?>>Kabupaten Bandung</option>
                        <option value="Kota Bandung" <?php if (!empty($response['provinsi']) && $response['provinsi'] == 'Kota Bandung')  echo 'selected = "selected"'; ?>>Kota Bandung</option>
                    </optgroup>
                    <optgroup label="DKI Jakarta">
                        <option value="Jakarta Utara" <?php if (!empty($response['provinsi']) && $response['provinsi'] == 'Jakarta Utara')  echo 'selected = "selected"'; ?>>Jakarta Utara</option>
                        <option value="Jakarta Selatan" <?php if (!empty($response['provinsi']) && $response['provinsi'] == 'Jakarta Selatan')  echo 'selected = "selected"'; ?>>Jakarta Selatan</option>
                        <option value="Jakarta Barat" <?php if (!empty($response['provinsi']) && $response['provinsi'] == 'Jakarta Barat')  echo 'selected = "selected"'; ?>>Jakarta Barat</option>
                        <option value="Jakarta Timur" <?php if (!empty($response['provinsi']) && $response['provinsi'] == 'Jakarta Timur')  echo 'selected = "selected"'; ?>>Jakarta Timur</option>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="container" style="margin-top: 20px">
            <div class="row">
                <div class="col-md-1 col-md-offset-1 col-md-push-4">
                    <input class="btn btn-info " type="submit" name="submit" value="Update">
                </div>
            </div>
        </div>
    </form>
</div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="css/profile.js"></script>
<script src="css/editprofil.js"></script>
</body>
</html>

