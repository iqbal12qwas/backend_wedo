<?php

session_start();
if(!isset($_SESSION['username'])){
    echo "<script>window.alert('You Must Be Log In !')
    window.location='./login.php'</script>";
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/Logo.png"> 
    <title>Add Package</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="css/dashboard.css">

    <style type="text/css">
    input[type=file] {
        display: inline;
    }
    #image_preview{
        border: 1px solid black;
        padding: 10px;
        margin-top: 10px;
    }
    #image_preview img{
        border: 1px solid black;
        width: 150px;
        height: 150px;
        padding: 5px;
    }
</style>
</head>

<body>
   <?php
   include 'navbar.php';
   ?>
   <div class="container">
    <form action="api/post/inputpackage.php" class="form-horizontal" method="post" enctype="multipart/form-data" >
        <div class="row">
            <h2 style="text-align: center; margin-bottom: 40px; color: #07E5F4;">INPUT PACKAGE</h2>
        </div>
        <div class="form-group row">
            <label for="option" class="col-sm-2 col-form-label">Option :</label>
            <div class="col-sm-3">
                <select class="form-control" name="option" >
                    <optgroup label="Option">
                        <option value="Package" >Package</option>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="category" class="col-sm-2 col-form-label">Category :</label>
            <div class="col-sm-3">
                <select class="form-control" name="category">
                    <optgroup label="Category">
                        <option value="Package">Package</option>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="type" class="col-sm-2 col-form-label">Type Item :</label>
            <div class="col-sm-3">
                <select class="form-control" name="typesp" >
                    <optgroup label="Type Item">
                        <option value="Package">Package</option>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="type" class="col-sm-2 col-form-label">Category Of Package :</label>
            <div class="col-sm-3">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Wedding Venue">Wedding Venue
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Bridal Fashion">Bridal Fashion
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Decoration"> Decoration
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Hair and Makeup and Wellness">Hair ,Makeup, &amp; Wellness
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Photographers">Photographers
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Videographers">Videographers
                    </label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Flowers and Floral">Flowers &amp; Floral
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Invitations">Invitations
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Entertainment"> Entertainment
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Souvernirs">Souvernirs
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Cathering">Cathering
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Wedding Cakes and Desert">Wedding Cakes &amp; Desert
                    </label>
                </div>
                <div class="checkbox" style="display: none;">
                    <label>
                        <input type="checkbox" name="copackage[]" value="Package" checked="active">Package
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name :</label>
            <div class="col-sm-3">
                <input class="form-control" type="text" name="namesp"  placeholder="Name Package" maxlength="20" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label">Price :</label>
            <div class="col-sm-3">
                <input class="form-control" type="text" name="price"  placeholder="Price" inputmode="numeric" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description :</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="5" name="description" placeholder="Description" required></textarea>
            </div>
        </div>
        <h6 style="color: #FD1C1C;"><strong>*Upload Your Images/Pictures</strong></h6>
        <input class="form-control" type="file" id="picture" name="picture[]" multiple required>
        <div id="image_preview" style="margin-bottom: 10px;"></div>
        <div class="col-md-1 col-md-offset-1 col-md-push-5">
            <input class="btn btn-info" type="submit" name="submit" value="Finish">
        </div>
    </form>
</div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="css/additem.js"></script>
</body>

<script type="text/javascript">

    $('#picture').change(function(){
        $('#image_preview').html("");
        var total_file = document.getElementById("picture").files.length;

        for(var i=0; i<total_file;i++) {
            $('#image_preview').append("<img src = '"+URL.createObjectURL(event.target.files[i])+"'>");
        }
    });

    $('form').ajaxForm(function() {
        alert("Uploaded SuccessFully");
    })
</script>

</html>