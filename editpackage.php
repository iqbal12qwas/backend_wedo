<?php

session_start();
if(!isset($_SESSION['username'])){
	echo "<script>window.alert('You Must Be Log In !')
	window.location='./login.php'</script>";
}

$_GET['pi'];
$_GET['iv'];
$_GET['o'];
$cp = explode(", ",$_GET['cp']);
$_GET['t'];
$_GET['c'];
$_GET['tt'];
$_GET['n'];
$_GET['pr'];
$_GET['des'];
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="assets/img/Logo.png"> 
	<title>Edit Package</title>
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/styles.css">
	<link rel="stylesheet" href="css/dashboard.css">
	<link rel="stylesheet" href="css/editprofil.css">

	<style type="text/css">
	input[type=file] {
		display: inline;
	}
	#image_preview{
		border: 1px solid black;
		padding: 10px;
	}
	#image_preview img{
		border: 1px solid black;
		width: 150px;
		height: 150px;
		padding: 5px;
	}
	#preview{
		display: inline;
		padding: 10px;
	}
	#preview img{
		border: 1px solid black;
		width: 150px;
		height: 150px;
		padding: 5px;
		margin-bottom: 20px;
	}
</style>
</head>

<body>
	<?php
	include 'navbar.php';
	?>
	<div class="container">
		<div class="row">
			<div class="text-h1">
				<p class="text-p">Edit Package</p>
			</div>
		</div>
	</div>
	<div class="container" >
		<?php
		
		require_once 'includes/config_pictureitemsandpackages.php';

		$id_product = new MongoDB\BSON\ObjectId($_GET['pi']);
		$qry2 = array('$and' => array(array('id_product' => $id_product), array('id_vendor' => $_SESSION['id'])));
		$response2 = $collection3->find($qry2);
		foreach ($response2 as $id2 => $key2){
			echo '<div id="preview"><img style="width:150px;height:150px" alt="" src="upload/product/'.$key2['picture'].'">  </div>';

		}
		?>
		<form action="api/path/updateimagepackage.php" class="form-horizontal" method="post" enctype="multipart/form-data" >
			<div class="form-group row">
				<h6 style="color: #FD1C1C;"><strong>*Upload Your Images/Pictures</strong></h6>
				<input class="form-control" type="file" accept=".png,.jpg,.jpeg,.gif" id="picture" name="picture[]" multiple required>
				<input class="form-control" type="text" required="" name="id" value="<?php echo $_GET['pi'];?>" style="display: none;">
				<input class="form-control" type="text" required="" name="id_vendor" value="<?php echo $_GET['iv'];?>" style="display: none;">
				<div id="image_preview" style="margin-bottom: 10px;"></div>
				<div class="col-md-1 col-md-offset-1 col-md-push-5">
					<input class="btn btn-info " type="submit" name="submit" value="Update Picture">
				</div>
			</div>
		</form>
		<form action="api/path/updatepackage.php" class="form-horizontal" method="post" enctype="multipart/form-data" >
			<div class="form-group row">
				<label for="option" class="col-sm-2 col-form-label">Option :</label>
				<div class="col-sm-3">
					<select class="form-control" name="option">
						<optgroup label="Option">
							<option value="Package">Package</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="category" class="col-sm-2 col-form-label">Category :</label>
				<div class="col-sm-3">
					<select class="form-control" name="category">
						<optgroup label="Category">
							<option value="Package">Package</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="type" class="col-sm-2 col-form-label">Type Item :</label>
				<div class="col-sm-3">
					<select class="form-control" name="typesp">
						<optgroup label="Type Item">
							<option value="Package">Package</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="type" class="col-sm-2 col-form-label">Category Of Package :</label>
				<div class="col-sm-3">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Wedding Venue" <?php if(in_array("Wedding Venue",$cp)) echo 'checked="checked"'; ?>>Wedding Venue
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Bridal Fashion" <?php if(in_array("Bridal Fashion",$cp)) echo 'checked="checked"'; ?>>Bridal Fashion
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Decoration" <?php if(in_array("Decoration",$cp)) echo 'checked="checked"'; ?>> Decoration
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Hair and Makeup and Wellness" <?php if(in_array("Hair and Makeup and Wellness",$cp)) echo 'checked="checked"'; ?>>Hair ,Makeup, &amp; Wellness
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Photographers" <?php if(in_array("Photographers",$cp)) echo 'checked="checked"'; ?>>Photographers
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Videographers" <?php if(in_array("Videographers",$cp)) echo 'checked="checked"'; ?>>Videographers
						</label>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Flowers and Floral" <?php if(in_array("Flowers and Floral",$cp)) echo 'checked="checked"'; ?>>Flowers &amp; Floral
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Invitations" <?php if(in_array("Invitations",$cp)) echo 'checked="checked"'; ?>>Invitations
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Entertainment" <?php if(in_array("Entertainment",$cp)) echo 'checked="checked"'; ?>> Entertainment
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Souvernirs" <?php if(in_array("Souvernirs",$cp)) echo 'checked="checked"'; ?>>Souvernirs
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Cathering" <?php if(in_array("Cathering",$cp)) echo 'checked="checked"'; ?>>Cathering
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="copackage[]" value="Wedding Cakes and Desert" <?php if(in_array("Wedding Cakes and Desert",$cp)) echo 'checked="checked"'; ?>>Wedding Cakes &amp; Desert
						</label>
					</div>
					<div class="checkbox" style="display: none;">
						<label>
							<input type="checkbox" name="copackage[]" value="Package" checked="active" <?php if(in_array("Package",$cp)) echo 'checked="checked"'; ?>>Package
						</label>
					</div>
				</div>
			</div>
			<div class="form-group row" id="ifYes" style="display: none;">
				<label for="type_time" class="col-sm-2 col-form-label">Type Time :</label>
				<div class="col-sm-3">
					<select class="form-control" name="type_time">
						<optgroup label="Type Time">
							<option value="Days">Days</option>
							<option value="Hours">Hours</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-sm-2 col-form-label">Name :</label>
				<div class="col-sm-3">
					<input class="form-control" type="text" name="namesp"  placeholder="Name Service/Product" value="<?php echo $_GET['n'];?>" required>
					<input class="form-control" type="text" required="" name="id" value="<?php echo $_GET['pi'];?>" style="display: none;">
				</div>
			</div>
			<div class="form-group row">
				<label for="price" class="col-sm-2 col-form-label">Price :</label>
				<div class="col-sm-3">
					<input class="form-control" type="text" name="price" value="<?php echo number_format($_GET['pr'], 0, ".", "."); ?>" placeholder="Price" inputmode="numeric" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" required>
				</div>
			</div>
			<div class="form-group row">
				<label for="description" class="col-sm-2 col-form-label">Description :</label>
				<div class="col-sm-10">
					<?php
					$description = str_replace('<br />', "\n", $_GET['des']);
					echo "<textarea class='form-control' rows='5' name='description' placeholder='Description Service/Product' required>".$description."</textarea>";
					?>
				</div>
			</div>
			<div class="col-md-1 col-md-offset-1 col-md-push-5">
				<input class="btn btn-info" type="submit" name="submit" value="Update Data">
			</div>
		</form>
	</div>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="css/additem.js"></script>
</body>

<script type="text/javascript">

	$('#picture').change(function(){
		$('#image_preview').html("");
		var total_file = document.getElementById("picture").files.length;

		for(var i=0; i<total_file;i++) {
			$('#image_preview').append("<img src = '"+URL.createObjectURL(event.target.files[i])+"'>");
		}
	});

	$('form').ajaxForm(function() {
		alert("Uploaded SuccessFully");
	})
</script>

<script type="text/javascript">

	function yesnoCheck(that) {
		if (that.value == "Rent") {
			document.getElementById("ifYes").style.display = "block";
		} else {
			document.getElementById("ifYes").style.display = "none";
		}
	}
</script>


</html>
