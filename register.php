<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/Logo.png"> 
    <title>Register</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/login.css">
    <link rel="stylesheet" href="assets/css/register.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div><img class="img-responsive" src="assets/img/Logo.png" style="margin-top: 50px;" width="100" height="100">
                <h1>Vendor Register Panel</h1></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-md-offset-5" ><a class="btn btn-info" role="button" href="login.php" >Log In</a></div>
            </div>
        </div>
        <form action="api/post/inputregister.php" method="POST">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-push-4">
                        <p class="text-center">Email </p>
                    </div>
                    <div class="col-lg-2 col-md-push-4">
                        <input class="form-control" type="email" required="" name="email">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-push-4">
                        <p class="text-center">Username </p>
                    </div>
                    <div class="col-lg-2 col-md-push-4">
                        <input class="form-control" type="text" required="" name="uname">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-push-4">
                        <p class="text-center">Your Name</p>
                    </div>
                    <div class="col-lg-2 col-md-push-4">
                        <input class="form-control" type="text" required="" name="name">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-push-4">
                        <p class="text-center">Provinsi </p>
                    </div>
                    <div class="col-lg-2 col-md-push-4">
                        <select class="form-control input-sm" name="provinsi">
                            <optgroup label="Jawa Barat">
                                <option value="Kabupaten Bandung">Kabupaten Bandung</option>
                                <option value="Kota Bandung">Kota Bandung</option>
                            </optgroup>
                            <optgroup label="DKI Jakarta">
                                <option value="Jakarta Utara">Jakarta Utara</option>
                                <option value="Jakarta Selatan">Jakarta Selatan</option>
                                <option value="Jakarta Barat">Jakarta Barat</option>
                                <option value="Jakarta Timur">Jakarta Timur</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-push-4">
                        <p class="text-center">Password </p>
                    </div>
                    <div class="col-lg-2 col-md-push-4">
                        <input class="form-control" type="password" required="" name="password" id="password" onkeyup="check();">
                        <input type="checkbox" onclick="myFunction1()"> Show Password
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-push-4">
                        <p class="text-center">Confirm Password</p>
                    </div>
                    <div class="col-lg-2 col-md-push-4">
                        <input class="form-control" type="password" required="" name="confirm_password" id="confirm_password" onkeyup="check();">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 col-md-offset-1 col-md-push-5">
                    <span id="message"></span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-1 col-md-offset-1 col-md-push-4">
                    <button class="btn btn-info btn-lg" type="submit" name="submit" style="margin-left: 30px; margin-top: 10px;">Finish </button>
                </div>
            </div>
        </div>
    </form>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="css/profile.js"></script>
    <script type="text/javascript">
        var check = function() {
            if (document.getElementById('password').value == document.getElementById('confirm_password').value) {
                document.getElementById('message').style.color = 'green';
                document.getElementById('message').innerHTML = 'Matching';
            } else {
                document.getElementById('message').style.color = 'red';
                document.getElementById('message').innerHTML = 'Not Matching';
            }
        }


        function myFunction1() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
    </script>
</body>

</html>

