-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2019 at 08:17 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wedo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_data_revenue`
--

CREATE TABLE `tbl_data_revenue` (
  `id` int(100) NOT NULL,
  `id_product` text NOT NULL,
  `id_customer` text NOT NULL,
  `id_vendor` text NOT NULL,
  `name` varchar(100) NOT NULL,
  `no_hp` text NOT NULL,
  `option` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `order` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `date_done` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `booking` date NOT NULL,
  `price` bigint(100) NOT NULL,
  `unit` int(100) NOT NULL,
  `duration` int(100) NOT NULL,
  `type_duration` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `paid` bigint(100) NOT NULL,
  `total` bigint(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(100) NOT NULL,
  `id_product` varchar(100) NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `id_vendor` text NOT NULL,
  `name` varchar(100) NOT NULL,
  `no_hp` text NOT NULL,
  `option` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `order` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `date_transaction` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `booking` varchar(50) NOT NULL,
  `price` bigint(100) NOT NULL,
  `unit` int(50) NOT NULL,
  `duration` int(50) NOT NULL,
  `type_duration` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `paid` bigint(100) NOT NULL,
  `paid_before` bigint(100) NOT NULL,
  `minus` bigint(100) NOT NULL,
  `total` bigint(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `pax` varchar(50) NOT NULL,
  `total_pax` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `id_product`, `id_customer`, `id_vendor`, `name`, `no_hp`, `option`, `type`, `order`, `category`, `date_transaction`, `booking`, `price`, `unit`, `duration`, `type_duration`, `note`, `paid`, `paid_before`, `minus`, `total`, `status`, `pax`, `total_pax`) VALUES
(4, '5d51a8071439b40a5c001bbb', '1', '5d0f6141d282cb0994006b31', 'a', '123', 'Product', 'Sell', 'Kartu Cantik', 'Invitations', '2019-08-15 07:07:47', '2019/8/15', 10000, 100, 0, ' ', 'test', 500000, 0, 500000, 1000000, 'On Payment', 'No', 1),
(5, '5d52225c1439b40a5c001bbf', '2', '5d0f6141d282cb0994006b31', 'aa', '123', 'Product', 'Rent', 'Venue Megah', 'Wedding Venue', '2019-08-15 07:08:20', '2019/8/15', 100000, 0, 2, 'Days', 'test', 100000, 0, 100000, 200000, 'On Payment', 'Yes', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `id` int(100) NOT NULL,
  `id_order` text NOT NULL,
  `name` varchar(100) NOT NULL,
  `timestamp` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`id`, `id_order`, `name`, `timestamp`, `url`) VALUES
(1, '5d52225c1439b40a5c001bbf', 'test', '2019-01-01', 'uploads/payment_receipt/2019-08-12T18-06-30.694Z5d2c6842f3319c200c49d482_IMG_20190709_145007.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_review`
--

CREATE TABLE `tbl_review` (
  `id` int(100) NOT NULL,
  `id_product` text NOT NULL,
  `id_customer` text NOT NULL,
  `id_vendor` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `rating` float NOT NULL,
  `created_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_review`
--

INSERT INTO `tbl_review` (`id`, `id_product`, `id_customer`, `id_vendor`, `email`, `name`, `comment`, `rating`, `created_at`) VALUES
(9, '5d38050e1439b40a38001373', '5d2c6842f3319c200c49d482', '5d3803e41439b40a38001372', 'l@l.com', 'joni', 'bagus teh bunganya.. :) ', 5, '24 Jul 2019 - 14:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_schedule`
--

CREATE TABLE `tbl_schedule` (
  `id` int(100) NOT NULL,
  `id_vendor` text NOT NULL,
  `name` varchar(100) NOT NULL,
  `no_hp` text NOT NULL,
  `event` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `place` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_your_customer`
--

CREATE TABLE `tbl_your_customer` (
  `id` int(100) NOT NULL,
  `id_product` varchar(100) NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `id_vendor` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `option` varchar(50) NOT NULL,
  `order` varchar(100) NOT NULL,
  `category` varchar(50) NOT NULL,
  `price` bigint(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `type_duration` varchar(50) NOT NULL,
  `date_order` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `pax` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_your_customer`
--

INSERT INTO `tbl_your_customer` (`id`, `id_product`, `id_customer`, `id_vendor`, `name`, `no_hp`, `option`, `order`, `category`, `price`, `type`, `type_duration`, `date_order`, `status`, `pax`) VALUES
(1, '5d51a8071439b40a5c001bbb', '1', '5d0f6141d282cb0994006b31', 'a', '123', 'Product', 'Kartu Cantik', 'Invitations', 10000, 'Sell', '-', '2019-01-01', 'On Payment', 'No'),
(2, '5d52225c1439b40a5c001bbf', '2', '5d0f6141d282cb0994006b31', 'test', '123', 'Product', 'Venue Megah', 'Wedding Venue', 100000, 'Rent', 'Days', '2019-01-01', 'On Payment', 'Yes'),
(3, '5d52225c1439b40a5c001bbf', '2', '5d0f6141d282cb0994006b31', 'test', '123', 'Package', 'Paket Ekonomis', 'Package', 100000, 'Package', '-', '2019-01-01', 'On Payment', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_data_revenue`
--
ALTER TABLE `tbl_data_revenue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_review`
--
ALTER TABLE `tbl_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_your_customer`
--
ALTER TABLE `tbl_your_customer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_data_revenue`
--
ALTER TABLE `tbl_data_revenue`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_review`
--
ALTER TABLE `tbl_review`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_your_customer`
--
ALTER TABLE `tbl_your_customer`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
