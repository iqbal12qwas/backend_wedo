<?php
require_once 'includes/config.php';


if(isset($_POST['export'])) {
	$datestart = $_POST['date_start'];
	$resultstart = explode('-', $datestart);
	$startdate = $resultstart[2];
	$startmonth = $resultstart[1];
	$startyear = $resultstart[0];
	$newstart = $startyear.'-'.$startmonth.'-'.$startdate;

	$dateend = $_POST['date_end'];
	$resultend = explode('-', $dateend);
	$enddate = $resultend[2];
	$endmonth = $resultend[1];
	$endyear = $resultend[0];
	$newend = $endyear.'-'.$endmonth.'-'.$enddate;

	$qry = "SELECT * FROM `tbl_data_revenue` WHERE `booking` BETWEEN '$newstart' AND '$newend' ORDER BY `booking` ASC";
	$res = mysqli_query($con, $qry);
	$output = '';
	$i = 1;
	if (mysqli_num_rows($res) > 0) {
		$output .= '
		<table class="table" border="1">
		<tr>
		<th>No</th>
		<th>ID</th>
		<th>Name</th>
		<th>NoHp</th>
		<th>Option</th>
		<th>Type</th>
		<th>Order</th>
		<th>Category</th>
		<th>D.Done</th>
		<th>Booking</th>
		<th>Price(Rp)</th>
		<th>Unit</th>
		<th>Duration</th>
		<th>T.Time</th>
		<th>Note</th>
		<th>Paid(Rp)</th>
		<th>Total(Rp)</th>
		</tr>
		';
		while($row = mysqli_fetch_array($res)) {
			$output .= '
			<table class="table" border="1">
			<tr>
			<th>'.$i++.'</th>
			<th>'.$row["id"].'</th>
			<th>'.$row["name"].'</th>
			<th>'.$row["no_hp"].'</th>
			<th>'.$row["option"].'</th>
			<th>'.$row["type"].'</th>
			<th>'.$row["order"].'</th>
			<th>'.$row["category"].'</th>
			<th>'.$row["date_done"].'</th>
			<th>'.$row["booking"].'</th>
			<th>'.number_format($row["price"], 0, ".", ".").'</th>
			<th>'.$row["unit"].'</th>
			<th>'.$row["duration"].'</th>
			<th>'.$row["type_duration"].'</th>
			<th>'.nl2br($row["note"]).'</th>
			<th>'.number_format($row["paid"], 0, ".", ".").'</th>
			<th>'.number_format($row["total"], 0, ".", ".").'</th>
			</tr>
			';
		}
		$output .= '</table>';
		header("Content-Type: application/xls");
		header("Content-Disposition: attachment; filename=income.xls");
		echo $output;
	}
	
}
?>