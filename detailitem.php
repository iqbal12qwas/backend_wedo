
<?php

session_start();
if(!isset($_SESSION['username'])){
  echo "<script>window.alert('You Must Be Log In !')
  window.location='./login.php'</script>";
}


$_GET['pi'];
$_GET['iv'];
$_GET['o'];
$_GET['cp'];
$_GET['t'];
$_GET['p'];
$_GET['st'];
$_GET['c'];
$_GET['tt'];
$_GET['n'];
$_GET['pr'];
$_GET['des'];
$_GET['s'];
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="assets/img/Logo.png"> 
  <title>Detail Item</title>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="css/detail.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="assets/css/styles.css">
</head>


<body>
  <?php
  include 'navbar.php';
  ?>
  <div class="container-fluid text-center">    
    <div class="row content">
      <div class="col-sm-9 text-left"> 
       <div class="individual-car-title">
        <h3><strong><?php echo $_GET['n'];?></strong></h3>
        <h3 style="color:#FFD700">Rp. <?php echo number_format($_GET['pr'], 0, ".", "."); ?></h3>
      </div> 
      <div class="cars-gallery" style="margin-top: 20px;" >
       <div class="swiper-container gallery-top">
        <div class="swiper-wrapper">
          <?php
          require_once 'includes/config_itemsandpackages.php';
          require_once 'includes/config_pictureitemsandpackages.php';


          $id_product = new MongoDB\BSON\ObjectId($_GET['pi']);
          $qry2 = array('$and' => array(array('id_product' => $id_product), array('id_vendor' => $_SESSION['id'])));
          $response2 = $collection3->find($qry2);
          foreach ($response2 as $id2 => $key2){
            echo '<div class="swiper-slide"> 
            <div class="swiper-zoom-container">
            <img style="width:300px;height:300px" alt="" src="upload/product/'.$key2['picture'].'"/> 
            </div>
            </div>';

          }
          ?>

        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next swiper-button-blue"></div>
        <div class="swiper-button-prev swiper-button-blue"></div>
      </div>
      <div class="swiper-container gallery-thumbs" style="margin-top: 10px;">
        <div class="swiper-wrapper">
          <?php

          require_once 'includes/config_itemsandpackages.php';
          require_once 'includes/config_pictureitemsandpackages.php';
          require_once 'includes/config.php';
          $id_product = $_GET['pi'];
          $id_vendor = $_GET['iv'];

          $query1 = "SELECT ROUND(AVG(rating), 0) as rating FROM `tbl_review` WHERE `id_product` = '$id_product' AND `id_vendor` = '$id_vendor'";
          $result1 = mysqli_query($con, $query1);
          $row1 = mysqli_fetch_array($result1);

          $id_product = new MongoDB\BSON\ObjectId($_GET['pi']);
          $qry2 = array('$and' => array(array('id_product' => $id_product), array('id_vendor' => $_SESSION['id'])));
          $response2 = $collection3->find($qry2);
          foreach ($response2 as $id2 => $key2){
            echo '<div class="swiper-slide"> 
            <div class="swiper-zoom-container">
            <img style="width:70px; height:70px;" src="upload/product/'.$key2['picture'].'"> 
            </div>
            </div>';


          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <span class="thumbnail" style="margin-top: 220px;">
     <div class="menu sticky-top ">
      <div class="card bg-light ">
        <div class="card-body">
          <div class="cars-tabs">
            <div class="card">
              <div class="card-body">
                <div class="row text-left">
                  <h4><strong>Detail Product</strong></h4>
                  <ul>
                    <li><strong>Status : </strong><?php echo $_GET['s'];?></li>
                    <li><strong>Type : </strong><?php echo $_GET['t'];?></li>
                    <li><strong>Option : </strong><?php echo $_GET['o'];?></li>
                    <li><strong>Category : </strong><?php echo $_GET['c'];?></li>
                    <li><strong>Stock : </strong><?php echo $_GET['st'];?></li>
                    <li><strong>Pax : </strong><?php echo $_GET['p'];?></li>
                    <li style="width: 300px;"><strong>Category Package : </strong><?php echo $_GET['cp'];?></li>
                    <li><strong>Type Time : </strong><?php echo $_GET['tt'];?></li>
                    <li><strong>Rating (1-5) : </strong> <?php
                    for($i=1;$i<=$row1['rating'];$i++) {
                      echo '<span class="float-right"><i class="fa fa-star" style="color:blue"></i></span>';
                    }  
                    ?></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </span>
<?php 
if (($_GET['t']=='Package') && ($_GET['o']=='Package') && ($_GET['c']=='Package')){
  echo "<a href='editpackage.php?pi=$_GET[pi]&iv=$_GET[iv]&o=$_GET[o]&t=$_GET[t]&c=$_GET[c]&st=$_GET[st]&p=$_GET[p]&cp=$_GET[cp]&tt=$_GET[tt]&n=$_GET[n]&pr=$_GET[pr]&des=$_GET[des]' class='btn btn-primary'>Update Package</a>";
} else {
 echo "<a href='edititem.php?pi=$_GET[pi]&iv=$_GET[iv]&o=$_GET[o]&t=$_GET[t]&c=$_GET[c]&st=$_GET[st]&p=$_GET[p]&cp=$_GET[cp]&tt=$_GET[tt]&n=$_GET[n]&pr=$_GET[pr]&des=$_GET[des]' class='btn btn-primary'>Update Item</a>";
}
?>

<?php 
if (($_GET['t']=='Package') && ($_GET['o']=='Package') && ($_GET['c']=='Package')){
  echo "<a href='api/delete/deleteitem.php?pi=$_GET[pi]&iv=$_GET[iv]&o=$_GET[o]&t=$_GET[t]&c=$_GET[c]&st=$_GET[st]&p=$_GET[p]&tt=$_GET[tt]&n=$_GET[n]&pr=$_GET[pr]&des=$_GET[des]' class='btn btn-danger' onclick=\"return  confirm('Delete This Package ?')\">Delete Package</a>";
} else {
 echo "<a href='api/delete/deleteitem.php?pi=$_GET[pi]&iv=$_GET[iv]&o=$_GET[o]&t=$_GET[t]&c=$_GET[c]&st=$_GET[st]&p=$_GET[p]&tt=$_GET[tt]&n=$_GET[n]&pr=$_GET[pr]&des=$_GET[des]' class='btn btn-danger' onclick=\"return  confirm('Delete This Item ?')\">Delete Item</a>";
}
?>
</div>
<div class="col-md-12">
  <span class="thumbnail" style="margin-top: 20px;">
   <div class="menu sticky-top ">
    <div class="card bg-light ">
      <div class="card-body">
        <div class="cars-tabs">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <h4><strong>Description</strong></h4>
                </div>  
                <div class="col-md-12">

                  <?php
                  require_once 'includes/config_itemsandpackages.php';

                  $id_product = new MongoDB\BSON\ObjectId($_GET['pi']);
                  $qry2 = array('$and' => array(array('_id' => $id_product), array('id_vendor' => $_SESSION['id'])));
                  $response2 = $collection2->find($qry2);
                  foreach ($response2 as $id2 => $key2){
                    $description = str_replace('<br />', "\n", $key2['description']);
                    echo "<pre class='text-left' style='font-family: Arial; background-color: white;'>".$description."</pre>";
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</span>
</div>
<h4><strong>Review</strong></h4>
<?php
require_once 'includes/config.php';
require_once 'includes/config_uprofiles.php';
if(isset($_GET['pi']) and isset($_GET['iv'])) {

  $id_product = $_GET['pi'];
  $id_vendor = $_GET['iv'];
  $query = "SELECT * FROM `tbl_review` WHERE `id_product` = '$id_product' AND `id_vendor` = '$id_vendor'";
  $result = mysqli_query($con, $query);
  while($row = mysqli_fetch_array($result)){
    $comment=str_replace("\n","<br />",$row['comment']);
    $idc = $row['id_customer'];
    $id_customer = new MongoDB\BSON\ObjectId($idc);
    $qry = array('id_customer' => $id_customer);
    $response = $collection4->find($qry);
    foreach ($response as $get) {
      ?>

      <div class="col-md-12">
        <span class="thumbnail" style="margin-top: 5px;">
          <div class="card bg-light ">
            <div class="card-body">
              <div class="cars-tabs">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-2">
                       <img class="img img-rounded img-fluid" style="width:100px; height:100px;" src="http://118.97.217.254:8080/<?php echo $get['wedImg']; ?>">
                     </div>
                     <div class="col-md-10 text-left">
                      <p class="text-left"><i class="fa fa-clock-o"></i> <?php echo $row['created_at'] ?></p>
                      <a class="float-left" style="margin-right: 15px;"><strong><?php echo $row['name'] ?></strong></a>
                      <?php
                      for($i=1;$i<=$row['rating'];$i++) {
                        echo '<span class="float-right"><i class="fa fa-star" style="color:blue"></i></span>';
                      }  
                      ?>
                    </p>
                    <div class="well well-sm">
                      <pre class="text-left" style="font-family: Arial; background-color: white;"><?php echo $comment ?></pre>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </span>
  </div>
<?php }
}
}
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>
<script src="css/detail.js"></script>
</body>
</html>
