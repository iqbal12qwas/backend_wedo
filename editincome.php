<?php
include "includes/config.php";

if($_POST['rowid']) {

    $id = $_POST['rowid'];      

    $sql = mysqli_query($con, "SELECT * FROM `tbl_data_revenue` WHERE `id` = $id");

    while ($result = mysqli_fetch_array($sql)){
        $note = str_replace('<br />', "\n", $result['note']);

        ?>

        <form action="api/path/updateincome.php" method="post">
            <input type="hidden" name="id" value="<?php echo $result['id']; ?>">
            <input type="hidden" name="id_customer" value="<?php echo $result['id_customer']; ?>">
            <input type="hidden" name="id_vendor" value="<?php echo $result['id_vendor']; ?>">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="name" value="<?php echo $result['name']; ?>">
            </div>
            <div class="form-group">
                <label>No Handphone</label>
                <input type="text" class="form-control" onkeydown="return numbersonly(this, event);" name="no_hp" value="<?php echo $result['no_hp']; ?>">
            </div>
            <div class="form-group rows">
                <label>Option :</label>
                <select class="form-control" name="option">
                    <optgroup label="Option">
                        <option value="Product" <?php if (!empty($result['option']) && $result['option'] == 'Product')  echo 'selected = "selected"'; ?>>Product</option>
                        <option value="Service" <?php if (!empty($result['option']) && $result['option'] == 'Service')  echo 'selected = "selected"'; ?>>Service</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <label>Type Item</label>
                <select class="form-control" name="typesp" onchange="yesnoCheck(this);">
                    <optgroup label="Type Item">
                        <option value="Sell" <?php if (!empty($result['typesp']) && $result['typesp'] == 'Sell')  echo 'selected = "selected"'; ?>>Sell</option>
                        <option value="Rent" <?php if (!empty($result['typesp']) && $result['typesp'] == 'Rent')  echo 'selected = "selected"'; ?>>Rent</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <label>Order</label>
                <input type="text" class="form-control" name="order" value="<?php echo $result['order']; ?>">
            </div>
            <?php
            if($result['category'] == "Package"){
                echo "
                <div class='form-group'>
                <label>Category</label>
                <select class='form-control' name='category'>
                <optgroup label='Category'>
                <option value='Package'>Package</option>
                </optgroup>
                </select>
                </div>";
            } else {
                echo"<div class='form-group'>";?>
                <label>Category :</label>
                <select class="form-control" name="category">
                    <optgroup label="Category">
                        <option value="Wedding Venue" <?php if (!empty($result['category']) && $result['category'] == 'Wedding Venue')  echo 'selected = "selected"'; ?>>Wedding Venue</option>
                        <option value="Decoration" <?php if (!empty($result['category']) && $result['category'] == 'Decoration')  echo 'selected = "selected"'; ?>>Decoration</option>
                        <option value="Bridal Fashion" <?php if (!empty($result['category']) && $result['category'] == 'Bridal Fashion')  echo 'selected = "selected"'; ?>>Bridal Fashion</option>
                        <option value="Hair, Makeup, Wellness" <?php if (!empty($result['category']) && $result['category'] == 'Hair, Makeup, Wellness')  echo 'selected = "selected"'; ?>>Make Up</option>
                        <option value="Photographers" <?php if (!empty($result['category']) && $result['category'] == 'Photographers')  echo 'selected = "selected"'; ?>>Photographers</option>
                        <option value="Videographers" <?php if (!empty($result['category']) && $result['category'] == 'Videographers')  echo 'selected = "selected"'; ?>>Videographers</option>
                        <option value="Wedding Cakes & Desert" <?php if (!empty($result['category']) && $result['category'] == 'Wedding Cakes & Deser')  echo 'selected = "selected"'; ?>>Wedding Cakes & Desert</option>
                        <option value="Flowers & Floral" <?php if (!empty($result['category']) && $result['category'] == 'Flowers & Floral')  echo 'selected = "selected"'; ?>>Flowers & Floral</option>
                        <option value="Invitations" <?php if (!empty($result['category']) && $result['category'] == 'Invitations')  echo 'selected = "selected"'; ?>>Invitations</option>
                        <option value="Entertaiment" <?php if (!empty($result['category']) && $result['category'] == 'Entertaiment')  echo 'selected = "selected"'; ?>>Entertaiment</option>
                        <option value="Souvenirs" <?php if (!empty($result['category']) && $result['category'] == 'Souvenirs')  echo 'selected = "selected"'; ?>>Souvenirs</option>
                        <option value="Cathering" <?php if (!empty($result['category']) && $result['category'] == 'Cathering')  echo 'selected = "selected"'; ?>>Cathering</option>
                    </optgroup>
                </select>
            </div>
        <?php }
        ?>

        <div class="form-group">
            <label>Booking</label>
            <input type="date" class="form-control" name="booking" value="<?php echo $result['booking']; ?>">
        </div>
        <div class="form-group">
            <label>Price (Rp)</label>
            <input type="text" class="form-control" name="price" value="<?php echo number_format($result['price'], 0, ".", "."); ?>" inputmode="numeric" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
        </div>
        <div class="form-group">
            <label>Unit</label>
            <input type="text" class="form-control" name="unit" onkeydown="return numbersonly(this, event);"  value="<?php echo $result['unit']; ?>">
        </div>
        <div class="form-group">
            <label>Duration</label>
            <input type="text" class="form-control" name="duration" onkeydown="return numbersonly(this, event);"  value="<?php echo $result['duration']; ?>">
        </div>
        <div class="form-group" id="ifYes">
            <label>Type Time :</label>
            <select class="form-control" name="type_duration">
                <optgroup label="Type Time">
                    <option value="-" <?php if (!empty($result['type_duration']) && $result['type_duration'] == '-')  echo 'selected = "selected"'; ?>>-</option>
                    <option value="Days" <?php if (!empty($result['type_duration']) && $result['type_duration'] == 'Days')  echo 'selected = "selected"'; ?>>Days</option>
                    <option value="Hours" <?php if (!empty($result['type_duration']) && $result['type_duration'] == 'Hours')  echo 'selected = "selected"'; ?>>Hours</option>
                </optgroup>
            </select>
        </div>
        <div class="form-group">
            <label>Note :</label>
            <?php echo "<textarea class='form-control' rows='3' name='note' placeholder='Note Service/Product' required>".$note."</textarea>";?>
        </div>
        <div class="form-group">
            <label>Paid (Rp)</label>
            <input type="text" class="form-control" name="paid" value="<?php echo number_format($result['paid'], 0, ".", "."); ?>" inputmode="numeric" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
        </div>
        <div class="form-group">
            <label>Total (Rp)</label>
            <input type="text" class="form-control" name="total" value="<?php echo number_format($result['total'], 0, ".", "."); ?>" inputmode="numeric" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
        </div>
        <button class="btn btn-info" type="submit" name="submit">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
    </form>     

<?php } }
?>
<script src="css/profile.js"></script>
<script type="text/javascript">

    function yesnoCheck(that) {
        if (that.value == "Rent") {
          document.getElementById("ifYes").style.display = "block";
      } else {
          document.getElementById("ifYes").style.display = "none";
      }
  }
</script>