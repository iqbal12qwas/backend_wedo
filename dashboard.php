<?php

session_start();
if(!isset($_SESSION['username'])){
    echo "<script>window.alert('You Must Be Log In !')
    window.location='./login.php'</script>";
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/Logo.png"> 
    <title>Dashboard</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
    <?php
    include 'navbar.php';
    ?>
    <div class="container">
        <form action="dashboard.php" method="get">
            <img class="img-thumbnail" src="assets/img/MB__search.png" width="40" height="40">
            <label class="control-label">Search : </label>
            <input class="form-control" type="text" name="search" id="search" placeholder="Name by Item/Package" autocomplete="off">
            <div id="namelist" ></div>
        </form>
    </div>
    <div class="container" style="margin-top:20px">
        <div class="row">
            <!-- BEGIN PRODUCTS -->
            <?php
            require_once 'includes/config_itemsandpackages.php';
            require_once 'includes/config_pictureitemsandpackages.php';

            if (isset($_GET['search'])){
                $search = $_GET['search'];
                $qry2 = array( '$and' => array(
                    array( '$or' => 
                        array( 
                            array( 'name' => new MongoDB\BSON\Regex($search))
                        )
                    ),
                    array( 'id_vendor' => $_SESSION['id']) 
                ));
                $response2 = $collection2->find($qry2);

                foreach ($response2 as $iap2){

                    $a_id_product2 = $iap2['_id'];


                    $qry3 = array('$and' => array(array('id_product' => $a_id_product2), array('id_vendor' => $_SESSION['id'])));
                    $response3 = $collection3->find($qry3, ['limit' => 1]);
                    foreach ($response3 as $id3 => $key3){


                       ?>
                       <div class="col-md-3 col-sm-6">
                        <span class="thumbnail">
                            <?php

                            echo '<img style="width:150px;height:150px" alt="" src="upload/product/'.$key3['picture'].'">';
                            ?>
                            <h5 style="text-align: center;"><?php echo $iap2['name'] ?></h5>
                            <p>Option : <?php echo $iap2['option'] ?> </p>
                            <p>Type : <?php echo $iap2['type'] ?> </p>
                            <p>Status : <?php echo $iap2['status']  ?> </p>
                            <p style="width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Category : <?php echo $iap2['category'] ?> </p>
                            <hr class="line">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <p class="price">Rp. <?php echo number_format($iap2['price'], 0, ".", "."); ?></p>
                                    <p class="stock">Stock : <?php echo $iap2['stock']; ?></p>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <?php 
                                    echo "<a href='detailitem.php?pi=$iap2[_id]&iv=$_SESSION[id]&o=$iap2[option]&t=$iap2[type]&c=$iap2[category]&st=$iap2[stock]&p=$iap2[pax]&cp=$iap2[category_package]&tt=$iap2[type_duration]&n=$iap2[name]&pr=$iap2[price]&des=$iap2[description]&s=$iap2[status]'  class='btn btn-info right'>View Detail</a>"
                                    ?>
                                </div>

                            </div>
                        </span>
                    </div>

                <?php } 
            }
        } else {

            $qry = array('id_vendor' => $_SESSION['id']);
            $response = $collection2->find($qry);
            foreach ($response as $iap){
                $a_id_product =  new MongoDB\BSON\ObjectId($iap['_id']);

                $qry2 = array('$and' => array(array('id_product' => $a_id_product), array('id_vendor' => $_SESSION['id'])));
                $response2 = $collection3->find($qry2, ['limit' => 1]);


                foreach ($response2 as $id2 => $key2){
                    ?>
                    <div class="col-md-3 col-sm-6">
                        <span class="thumbnail">
                            <?php
                            echo '<img style="width:150px;height:150px" alt="" src="upload/product/'.$key2['picture'].'">';
                            ?>
                            <h5 style="text-align: center;"><?php echo $iap['name'] ?></h5>
                            <p>Status : <?php echo $iap['status']  ?> </p>
                            <p>Option : <?php echo $iap['option'] ?> </p>
                            <p>Type : <?php echo $iap['type'] ?> </p>
                            <p style="width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">Category : <?php echo $iap['category'] ?> </p>
                            <hr class="line">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <p class="price">Rp. <?php echo number_format($iap['price'], 0, ".", "."); ?></p>
                                    <p class="stock">Stock : <?php echo $iap['stock']; ?></p>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <?php 
                                    echo "<a href='detailitem.php?pi=$iap[_id]&iv=$_SESSION[id]&o=$iap[option]&t=$iap[type]&c=$iap[category]&st=$iap[stock]&p=$iap[pax]&cp=$iap[category_package]&tt=$iap[type_duration]&n=$iap[name]&pr=$iap[price]&des=$iap[description]&s=$iap[status]'  class='btn btn-info right'>View Detail</a>"
                                    ?>
                                </div>

                            </div>
                        </span>
                    </div>
                <?php } 
            }
        } ?>

        <!-- END PRODUCTS -->
    </div>
</div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script>  
 $(document).ready(function(){  
  $('#search').keyup(function(){  
   var query = $(this).val();  
   if(query != '')  
   {  
    $.ajax({  
     url:"search.php",  
     method:"POST",  
     data:{query:query},  
     success:function(data)  
     {  
      $('#namelist').fadeIn();  
      $('#namelist').html(data);  
  }  
});  
}
});  
  $(document).on('click', 'li', function(){  
   $('#search').val($(this).text());  
   $('#namelist').fadeOut();  
});
});  
</script>
<script>
    $(window).ready(function(){
        $('body').click(function(){
            $("#namelist").hide();
        });
    });
</script>  



</body>

</html>