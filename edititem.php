<?php

session_start();
if(!isset($_SESSION['username'])){
	echo "<script>window.alert('You Must Be Log In !')
	window.location='./login.php'</script>";
}

$_GET['pi'];
$_GET['iv'];
$_GET['o'];
$_GET['t'];
$_GET['p'];
$_GET['c'];
$_GET['st'];
$_GET['tt'];
$_GET['n'];
$_GET['pr'];
$_GET['des'];
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="assets/img/Logo.png"> 
	<title>Edit Item</title>
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/styles.css">
	<link rel="stylesheet" href="css/dashboard.css">
	<link rel="stylesheet" href="css/editprofil.css">

	<style type="text/css">
	input[type=file] {
		display: inline;
	}
	#image_preview{
		border: 1px solid black;
		padding: 10px;
	}
	#image_preview img{
		border: 1px solid black;
		width: 150px;
		height: 150px;
		padding: 5px;
	}
	#preview{
		display: inline;
		padding: 10px;
	}
	#preview img{
		border: 1px solid black;
		width: 150px;
		height: 150px;
		padding: 5px;
		margin-bottom: 20px;
	}
</style>
</head>

<body>
	<?php
	include 'navbar.php';
	?>
	<div class="container">
		<div class="row">
			<div class="text-h1">
				<p class="text-p">Edit Item</p>
			</div>
		</div>
	</div>
	<div class="container" >
		<?php
		
		require_once 'includes/config_pictureitemsandpackages.php';

		$id_product = new MongoDB\BSON\ObjectId($_GET['pi']);
		$qry2 = array('$and' => array(array('id_product' => $id_product), array('id_vendor' => $_SESSION['id'])));
		$response2 = $collection3->find($qry2);
		foreach ($response2 as $id2 => $key2){
			echo '<div id="preview"><img style="width:150px;height:150px" alt="" src="upload/product/'.$key2['picture'].'">  </div>';
		}
		?>
		<form action="api/path/updateimageitem.php" class="form-horizontal" method="post" enctype="multipart/form-data" >
			<div class="form-group row">
				<h6 style="color: #FD1C1C;"><strong>*Upload Your Images/Pictures</strong></h6>
				<input class="form-control" type="file" accept=".png,.jpg,.jpeg,.gif" id="picture" name="picture[]" multiple required>
				<input class="form-control" type="text" required="" name="id" value="<?php echo $_GET['pi'];?>" style="display: none;">
				<input class="form-control" type="text" required="" name="id_vendor" value="<?php echo $_GET['iv'];?>" style="display: none;">
				<div id="image_preview" style="margin-bottom: 10px;"></div>
				<div class="col-md-1 col-md-offset-1 col-md-push-5">
					<input class="btn btn-info" type="submit" name="submit" value="Update Picture">
				</div>
			</div>
		</form>
		<form action="api/path/updateitem.php" class="form-horizontal" method="post" enctype="multipart/form-data" >
			<div class="form-group row">
				<label for="option" class="col-sm-2 col-form-label">Option :</label>
				<div class="col-sm-3">
					<select class="form-control" name="option">
						<optgroup label="Option">
							<option value="Product" <?php if (!empty($_GET['o']) && $_GET['o'] == 'Product')  echo 'selected = "selected"'; ?>>Product</option>
							<option value="Service" <?php if (!empty($_GET['o']) && $_GET['o'] == 'Service')  echo 'selected = "selected"'; ?>>Service</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="category" class="col-sm-2 col-form-label">Category :</label>
				<div class="col-sm-3">
					<select class="form-control" name="category"  onchange="Check(this);">
						<optgroup label="Category">
							<option value="Wedding Venue" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Wedding Venue')  echo 'selected = "selected"'; ?>>Wedding Venue</option>
							<option value="Decoration" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Decoration')  echo 'selected = "selected"'; ?>>Decoration</option>
							<option value="Bridal Fashion" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Bridal Fashion')  echo 'selected = "selected"'; ?>>Bridal Fashion</option>
							<option value="Hair, Makeup, Wellness" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Hair, Makeup, and Wellness')  echo 'selected = "selected"'; ?>>Hair, Makeup, &amp; Wellness</option>
							<option value="Photographers" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Photographers')  echo 'selected = "selected"'; ?>>Photographers</option>
							<option value="Videographers" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Videographers')  echo 'selected = "selected"'; ?>>Videographers</option>
							<option value="Wedding Cakes & Desert" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Wedding Cakes and Desert')  echo 'selected = "selected"'; ?>>Wedding Cakes &amp; Desert</option>
							<option value="Flowers & Floral" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Flowers and Floral')  echo 'selected = "selected"'; ?>>Flowers &amp; Floral</option>
							<option value="Invitations" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Invitations')  echo 'selected = "selected"'; ?>>Invitations</option>
							<option value="Entertainment" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Entertainment')  echo 'selected = "selected"'; ?>>Entertainment</option>
							<option value="Souvenirs" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Souvenirs')  echo 'selected = "selected"'; ?>>Souvenirs</option>
							<option value="Catering" <?php if (!empty($_GET['c']) && $_GET['c'] == 'Catering')  echo 'selected = "selected"'; ?>>Catering</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group row" id="Yes"  <?php if("Yes" == $_GET['p']){ echo 'style="display: block"';} else { echo 'style="display: none"'; }; ?>>
				<label for="type" class="col-sm-2 col-form-label">Pax :</label>
				<div class="col-sm-6">
					<div class="checkbox">
						<label>
							<input type="hidden" name="pax" value="No">
							<input type="checkbox" id="pax" name="pax" value="Yes" <?php if("Yes" == $_GET['p']) echo 'checked="checked"'; ?>>(Yes/No)
						</label>
						<label style="color: red;"><strong>
							(If Checked then this Item Price Considered Price/Pax)
						</strong></label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label for="type" class="col-sm-2 col-form-label">Type Item :</label>
				<div class="col-sm-3">
					<select class="form-control" id="typesp" name="typesp" onchange="yesnoCheck(this);">
						<optgroup label="Type Item">
							<option value="Sell" <?php if (!empty($_GET['t']) && $_GET['t'] == 'Sell')  echo 'selected = "selected"'; ?>>Sell</option>
							<option value="Rent" <?php if (!empty($_GET['t']) && $_GET['t'] == 'Rent')  echo 'selected = "selected"'; ?>>Rent</option>
						</optgroup>
					</select>
				</div>
			</div>
			<?php 
			if($_GET['t'] == "Rent"){
				?>
				<div class="form-group row" id="ifYes" style="display: block">
					<label for="type_time" class="col-sm-2 col-form-label">Type Time :</label>
					<div class="col-sm-3">
						<select class="form-control" name="type_duration">
							<optgroup label="Type Time">
								<option value="-" <?php if (!empty($_GET['tt']) && $_GET['tt'] == '-')  echo 'selected = "selected"'; ?>>-</option>
								<option value="Days" <?php if (!empty($_GET['tt']) && $_GET['tt'] == 'Days')  echo 'selected = "selected"'; ?>>Days</option>
								<option value="Hours" <?php if (!empty($_GET['tt']) && $_GET['tt'] == 'Hours')  echo 'selected = "selected"'; ?>>Hours</option>
							</optgroup>
						</select>
					</div>
				</div>
				<div class="form-group row" id="stock" style="display: none">
					<label for="stock" class="col-sm-2 col-form-label">Stock :</label>
					<div class="col-sm-3">
						<input class="form-control" type="text" name="stock" placeholder="Stock Product" value="<?php echo $_GET['st'];?>">
					</div> 
				</div>
				<?php
			} else {
				?>
				<div class="form-group row" id="ifYes" style="display: none">
					<label for="type_time" class="col-sm-2 col-form-label">Type Time :</label>
					<div class="col-sm-3">
						<select class="form-control" name="type_duration">
							<optgroup label="Type Time">
								<option value="-" <?php if (!empty($_GET['tt']) && $_GET['tt'] == '-')  echo 'selected = "selected"'; ?>>-</option>
								<option value="Days" <?php if (!empty($_GET['tt']) && $_GET['tt'] == 'Days')  echo 'selected = "selected"'; ?>>Days</option>
								<option value="Hours" <?php if (!empty($_GET['tt']) && $_GET['tt'] == 'Hours')  echo 'selected = "selected"'; ?>>Hours</option>
							</optgroup>
						</select>
					</div>
				</div>
				<div class="form-group row" id="stock" style="display: block">
					<label for="stock" class="col-sm-2 col-form-label">Stock :</label>
					<div class="col-sm-3">
						<input class="form-control" type="text" name="stock" placeholder="Stock Product" value="<?php echo $_GET['st'];?>">
					</div>
				</div>
				<?php
			}
			?>
			
			<div class="form-group row">
				<label for="name" class="col-sm-2 col-form-label">Name :</label>
				<div class="col-sm-3">
					<input class="form-control" type="text" name="namesp"  placeholder="Name Service/Product" value="<?php echo $_GET['n'];?>" required>
					<input class="form-control" type="text" required="" name="id" value="<?php echo $_GET['pi'];?>" style="display: none;">
				</div>
			</div>
			<div class="form-group row">
				<label for="price" class="col-sm-2 col-form-label">Price :</label>
				<div class="col-sm-3">
					<input class="form-control" type="text" name="price" value="<?php echo number_format($_GET['pr'], 0, ".", "."); ?>" placeholder="Price" inputmode="numeric" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" required>
				</div>
				<div class="col-sm-1" id="show" <?php if("Yes" == $_GET['p']) { echo 'style="display: block"'; } else { echo 'style="display: none"'; } ?>>
					<h5 style="color: red;"><strong>
						/ Pax
					</strong></h5>
				</div>
			</div>
			<div class="form-group row">
				<label for="description" class="col-sm-2 col-form-label">Description :</label>
				<div class="col-sm-10">
					<?php
					require_once 'includes/config_itemsandpackages.php';

					$id_product = new MongoDB\BSON\ObjectId($_GET['pi']);
					$qry2 = array('$and' => array(array('_id' => $id_product), array('id_vendor' => $_SESSION['id'])));
					$response2 = $collection2->find($qry2);
					foreach ($response2 as $id2 => $key2){
						$description = str_replace('<br />', "\n", $key2['description']);
						echo "<textarea class='form-control' rows='5' name='description' placeholder='Description Service/Product' required>".$description."</textarea>";
					}
					?>
				</div>
			</div>
			<div class="col-md-1 col-md-offset-1 col-md-push-5">
				<input class="btn btn-info" type="submit" name="submit" value="Update Data">
			</div>
		</form>
	</div>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="css/additem.js"></script>
</body>

<script type="text/javascript">

	$('#picture').change(function(){
		$('#image_preview').html("");
		var total_file = document.getElementById("picture").files.length;

		for(var i=0; i<total_file;i++) {
			$('#image_preview').append("<img src = '"+URL.createObjectURL(event.target.files[i])+"'>");
		}
	});

	$('form').ajaxForm(function() {
		alert("Uploaded SuccessFully");
	})
</script>


<script type="text/javascript">

	function yesnoCheck(that) {
		if (that.value == "Rent") {
			document.getElementById("ifYes").style.display = "block";
			document.getElementById("stock").style.display = "none";
		} else {
			document.getElementById("ifYes").style.display = "none";
			document.getElementById("stock").style.display = "block";
		}

		if (that.value == "Sell") {
			document.getElementById("stock").style.display = "block";
			document.getElementById("ifYes").style.display = "none";
		} else {
			document.getElementById("stock").style.display = "none";
			document.getElementById("ifYes").style.display = "block";
		}

		if (that.value == "-") {
			document.getElementById("stock").style.display = "none";
			document.getElementById("ifYes").style.display = "none";
		} 
	}

	function Check(that) {
		if ((that.value == "Wedding Venue") || (that.value == "Catering") || (that.value == "Souvenirs") || (that.value == "Invitations")) {
			document.getElementById("Yes").style.display = "block";
		} else {
			document.getElementById("Yes").style.display = "none";
		}
	}

	$(function () {
		$("#pax").click(function () {
			if ($(this).is(":checked")) {
				$("#show").show();
			} else {
				$("#show").hide();
			}
		});
	});

</script>

</html>
