<?php

require_once '../../includes/config.php';

if ($_SERVER['REQUEST_METHOD']=='POST'){
	$id_vendor = $_POST['id_vendor'];
	$key = $_POST['key'];
	if ($key != null) {
		$query = "SELECT * FROM `tbl_order` 
		WHERE (`id_vendor` = '$id_vendor')
		AND (`name` LIKE '%".$key."%'
		OR `no_hp` LIKE '%".$key."%'
		OR `order` LIKE '%".$key."%'
		OR `category` LIKE '%".$key."%'
		OR `status` LIKE '%".$key."%')";
		$result = mysqli_query($con, $query);
		$response = array();
		while($row = mysqli_fetch_array($result)){
			array_push($response,
				array(
					'id' => $row['id'],
					'id_product' => $row['id_product'],
					'id_customer' => $row['id_customer'],
					'id_vendor' => $row['id_vendor'],
					'name' => $row['name'],
					'no_hp' => $row['no_hp'],
					'option' => $row['option'],
					'type' => $row['type'],
					'order' => $row['order'],
					'category' => $row['category'],
					'date_transaction' => $row['date_transaction'],
					'booking' => $row['booking'],
					'price' => $row['price'],
					'unit' => $row['unit'],
					'duration' => $row['duration'],
					'type_duration' => $row['type_duration'],
					'note' => $row['note'],
					'paid_before' => $row['paid_before'],
					'paid' => $row['paid'],
					'minus' => $row['minus'],
					'total' => $row['total'],
					'status' => $row['status'],
					'pax' => $row['pax'],
					'total_pax' => $row['total_pax'])
			);
		}
		echo json_encode($response);	
	} else {
		$query = "SELECT * FROM `tbl_order` WHERE `id_vendor` = '$id_vendor' ";
		$result = mysqli_query($con, $query);
		$response = array();
		while($row = mysqli_fetch_array($result)){
			array_push($response, 
				array(
					'id' => $row['id'],
					'id_product' => $row['id_product'],
					'id_customer' => $row['id_customer'],
					'id_vendor' => $row['id_vendor'],
					'name' => $row['name'],
					'no_hp' => $row['no_hp'],
					'option' => $row['option'],
					'type' => $row['type'],
					'order' => $row['order'],
					'category' => $row['category'],
					'date_transaction' => $row['date_transaction'],
					'booking' => $row['booking'],
					'price' => $row['price'],
					'unit' => $row['unit'],
					'duration' => $row['duration'],
					'type_duration' => $row['type_duration'],
					'note' => $row['note'],
					'paid_before' => $row['paid_before'],
					'paid' => $row['paid'],
					'minus' => $row['minus'],
					'total' => $row['total'],
					'status' => $row['status'],
					'pax' => $row['pax'],
					'total_pax' => $row['total_pax'])
			);
		}
		echo json_encode($response);
	}
}
mysqli_close($con)
?>





