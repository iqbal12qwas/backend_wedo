<?php

require_once '../../includes/config_pictureitemsandpackages.php';

if (isset($_GET['id']) and isset($_GET['id_vendor'])) {
	$id_product = new MongoDB\BSON\ObjectId($_GET['id']);
	$id_vendor = new MongoDB\BSON\ObjectId($_GET['id_vendor']);

	$qry = array('$and' => array(array('id_product' => $id_product), array('id_vendor' => $id_vendor)));
	$row = $collection3->find($qry);
	$response = array();
	foreach ($row as $id => $key){
		array_push($response,
			array('picture' => $key['picture'])
		);
	}
	echo json_encode($response);

}
