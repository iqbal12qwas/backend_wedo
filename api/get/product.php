<?php

require_once '../../includes/config_itemsandpackages.php';
require_once '../../includes/config_pictureitemsandpackages.php';

if ($_SERVER['REQUEST_METHOD']=='POST'){
	$id_vendor = new MongoDB\BSON\ObjectId($_POST['id_vendor']);
	$search = $_POST['key'];
	if ($search != null) {
		$qry = array( '$and' => array(
			array( '$or' => 
				array( 
					array( 'name' => $search)
				)
			),
			array( 'id_vendor' => $id_vendor) 
		));
		$row = $collection2->find($qry);
		$response = array();
		foreach ($row as $id => $key){
			$id_product = new MongoDB\BSON\ObjectId($key['_id']);

			$qry2 = array('$and' => array(array('id_product' => $id_product), array('id_vendor' => $id_vendor)));
			$row2 = $collection3->find($qry2, ['limit' => 1]);
			foreach ($row2 as $id2 => $key2){

				array_push($response,
					array(
						'id' =>(string) $id_product,
						'id_vendor' =>(string) $id_vendor,
						'name_pic' => $key2['name_pic'],
						'picture' => $key2['picture'],
						'option' => $key['option'],
						'type' => $key['type'],
						'category' => $key['category'],
						'pax' => $key['pax'],
						'category_package' => $key['category_package'],
						'type_duration' => $key['type_duration'],
						'name' => $key['name'],
						'price' => $key['price'],
						'description' => $key['description'],
						'stock' => $key['stock'])
				);
			}
		}
		echo json_encode($response);
	} else {
		$qry = array('id_vendor' => $id_vendor);
		$row = $collection2->find($qry);
		$response = array();
		foreach ($row as $id => $key){
			$id_product =  new MongoDB\BSON\ObjectId($key['_id']);

			$qry2 = array('$and' => array(array('id_product' => $id_product), array('id_vendor' => $id_vendor)));
			$row2 = $collection3->find($qry2, ['limit' => 1]);
			
			foreach ($row2 as $id2 => $key2){
				array_push($response, 
					array(
						'id' =>(string) $id_product,
						'id_vendor' =>(string) $id_vendor,
						'name_pic' => $key2['name_pic'],
						'picture' => $key2['picture'],
						'option' => $key['option'],
						'type' => $key['type'],
						'category' => $key['category'],
						'pax' => $key['pax'],
						'category_package' => $key['category_package'],
						'type_duration' => $key['type_duration'],
						'name' => $key['name'],
						'price' => $key['price'],
						'description' => $key['description'],
						'stock' => $key['stock'])
				);
			}
		}
		echo json_encode($response);
	}

}
?>

