<?php

require_once '../../includes/config.php';
require_once '../../includes/config_itemsandpackages.php';

if ($_SERVER['REQUEST_METHOD']=='POST'){
	$id_vendor = $_POST['id_vendor'];
	$key = $_POST['key'];
	if ($key != null) {
		$query = "SELECT * FROM `tbl_your_customer` 
		WHERE (`id_vendor` = '$id_vendor')
		AND (`name` LIKE '%".$key."%'
		OR `no_hp` LIKE '%".$key."%'
		OR `order` LIKE '%".$key."%'
		OR `category` LIKE '%".$key."%') ORDER BY `tbl_your_customer`.`date_order` ASC";
		$result = mysqli_query($con, $query);
		$response = array();
		while($row = mysqli_fetch_array($result)){
			$query2 = array('_id' => new MongoDB\BSON\ObjectId($row['id_product']));
			$responses = $collection2->find($query2);
			foreach ($responses as $row2){
				array_push($response,
					array(
						'id' => $row['id'],
						'id_product' => $row['id_product'],
						'id_customer' => $row['id_customer'],
						'id_vendor' => $row['id_vendor'],
						'name' => $row['name'],
						'no_hp' => $row['no_hp'],
						'option' => $row['option'],
						'order' => $row['order'],
						'category' => $row['category'],
						'price' => $row['price'],
						'type' => $row['type'],
						'type_duration' => $row['type_duration'],
						'date_order' => $row['date_order'],
						'status' => $row['status'],
						'pax' => $row['pax'],
						'stock' => $row2['stock'])
				);
			}
		}
		echo json_encode($response);
	} else {
		$query = "SELECT * FROM `tbl_your_customer` WHERE `id_vendor` = '$id_vendor' ORDER BY `tbl_your_customer`.`date_order` ASC";
		$result = mysqli_query($con, $query);
		$response = array();
		while($row = mysqli_fetch_array($result)){
			$query2 = array('_id' => new MongoDB\BSON\ObjectId($row['id_product']));
			$responses = $collection2->find($query2);
			foreach ($responses as $row2){
				array_push($response, 
					array(
						'id' => $row['id'],
						'id_product' => $row['id_product'],
						'id_customer' => $row['id_customer'],
						'id_vendor' => $row['id_vendor'],
						'name' => $row['name'],
						'no_hp' => $row['no_hp'],
						'option' => $row['option'],
						'order' => $row['order'],
						'category' => $row['category'],
						'price' => $row['price'],
						'type' => $row['type'],
						'type_duration' => $row['type_duration'],
						'date_order' => $row['date_order'],
						'status' => $row['status'],
						'pax' => $row['pax'],
						'stock' => $row2['stock'])
				);
			}
		}	
		echo json_encode($response);
	}
}
mysqli_close($con)
?>

