	<?php

require_once '../../includes/config.php';

if ($_SERVER['REQUEST_METHOD']=='POST'){
	$id_vendor = $_POST['id_vendor'];
	$key = $_POST['key'];
	if ($key != null) {
		$query = "SELECT * FROM `tbl_schedule`
		WHERE (`id_vendor` = '$id_vendor')
		AND (`name` LIKE '%".$key."%'
		OR `no_hp` LIKE '%".$key."%'
		OR `event` LIKE '%".$key."%'
		OR `place` LIKE '%".$key."%') ORDER BY `tbl_schedule`.`date` ASC, `tbl_schedule`.`time` ASC";
		$result = mysqli_query($con, $query);
		$response = array();
		while($row = mysqli_fetch_array($result)){
			array_push($response,
				array(
					'id' => $row['id'],
					'id_vendor' => $row['id_vendor'],
					'name' => $row['name'],
					'no_hp' => $row['no_hp'],
					'event' => $row['event'],
					'date' => $row['date'],
					'time' => $row['time'],
					'place' => $row['place'])
			);
		}
		echo json_encode($response);
	} else {
		$query = "SELECT * FROM `tbl_schedule` WHERE `id_vendor` = '$id_vendor' ORDER BY `tbl_schedule`.`date` ASC, `tbl_schedule`.`time` ASC";
		$result = mysqli_query($con, $query);
		$response = array();
		while($row = mysqli_fetch_array($result)){
			array_push($response, 
				array(
					'id' => $row['id'],
					'id_vendor' => $row['id_vendor'],
					'name' => $row['name'],
					'no_hp' => $row['no_hp'],
					'event' => $row['event'],
					'date' => $row['date'],
					'time' => $row['time'],
					'place' => $row['place'])
			);
		}
		echo json_encode($response);
	}
}
mysqli_close($con)
?>

