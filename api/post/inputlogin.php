<?php
include ('../../includes/config_vendor.php');

if(isset($_POST['submit'])){

    $uname = $_POST['uname'];
    $password = $_POST['password'];
    $hashpw = md5($password);

    $terms =  array( '$and' => array(
        array( '$or' => 
            array( 
                array( 'uname' => $uname),
                array( 'email' => $uname)
            )
        ),
        array( 'password' => $hashpw) 
    ));
    $response = $collection->findOne($terms);

    $terms2 = array('_id' => $response['_id']);
    $response2 = $collection->findOne($terms2);

    
    if (($uname == $response2['uname']) or ($uname == $response2['email'])) {
        if ($hashpw == $response['password']) {
            if ($response) {
                session_start();
                $_SESSION['id'] = $response['_id'];
                $_SESSION['email'] = $response['email'];
                $_SESSION['username'] = $response['uname'];
                $_SESSION['password'] = $password;
                echo "<script>window.alert('Welcome ".$response['uname']."')
                window.location='../../dashboard.php'</script>";
            } else {
                echo "<script>window.alert('Can't Multiple Log In !')
                window.location='../../login.php'</script>";
            }
        } else {
            echo "<script>window.alert('Wrong Password !')
            window.location='../../login.php'</script>";
        }
    } else {
        echo "<script>window.alert('Wrong Username / Email & Password !')
        window.location='../../login.php'</script>";
    }
}