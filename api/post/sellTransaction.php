<?php

if($_SERVER['REQUEST_METHOD']=='POST'){

	$response = array();

	$id_product = $_POST['id_product'];
	$id_customer = $_POST['id_customer'];
	$id_vendor = $_POST['id_vendor'];
	$name = $_POST['name'];
	$no_hp = $_POST['no_hp'];
	$type = $_POST['type'];
	$order = $_POST['order'];
	$option = $_POST['option'];
	$category = $_POST['category'];
	$booking = $_POST['booking'];
	$price = $_POST['price'];
	$unit = $_POST['unit'];
	$note = $_POST['note'];
	$paid = $_POST['paid'];
	$minus = $_POST['minus'];
	$total = $_POST['total'];
	$pax = $_POST['pax'];
	$stock = $_POST['stock'];

	if ($minus == '0'){
		$statusIP = "Available";

		require_once '../../includes/config.php';
		require_once '../../includes/config_itemsandpackages.php';

		$sql = "INSERT INTO `tbl_order` (`id_product`, `id_customer`, `id_vendor`, `name`, `no_hp`, `option`, `type`, `order`, `category`, `date_transaction`, `booking`, `price`, `unit`, `duration`, `type_duration`, `note`, `paid`, `paid_before`, `minus`, `total`, `status`, `pax`, `total_pax`) VALUES ('$id_product', '$id_customer','$id_vendor', '$name', '$no_hp', '$option', '$type', '$order', '$category', CURRENT_TIMESTAMP, '$booking', '$price', '$unit', 0, ' ', '$note', '$paid', 0, '$minus', '$total', 'Completed', '$pax', 1);";
		if(mysqli_query($con,$sql)){
			$responses = $collection2->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id_product)),array('$set' => array('status' => $statusIP, 'stock' => $stock)));
			$response['value'] = 1;
			$response['message'] = "Input Successful";
			echo json_encode($response);
		} else {
			$response['value'] = 0;
			$response['message'] = "Input Failed";
			echo json_encode($response);
		}

	} else {
		$statusIP = "Not Available";

		require_once '../../includes/config.php';
		require_once '../../includes/config_itemsandpackages.php';

		$sql = "INSERT INTO `tbl_order` (`id_product`, `id_customer`, `id_vendor`, `name`, `no_hp`, `option`, `type`, `order`, `category`, `date_transaction`, `booking`, `price`, `unit`, `duration`, `type_duration`, `note`, `paid`, `paid_before`, `minus`, `total`, `status`, `pax`, `total_pax`) VALUES ('$id_product', '$id_customer', '$id_vendor', '$name', '$no_hp', '$option', '$type', '$order', '$category', CURRENT_TIMESTAMP, '$booking', '$price', '$unit', 0, ' ' , '$note', '$paid', 0, '$minus', '$total', 'On Payment', '$pax', 1);";
		if(mysqli_query($con,$sql)){
			$responses = $collection2->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id_product)),array('$set' => array('status' => $statusIP, 'stock' => $stock)));
			$response['value'] = 1;
			$response['message'] = "Input Successful";
			echo json_encode($response);
		} else {
			$response['value'] = 0;
			$response['message'] = "Input Failed";
			echo json_encode($response);
		}
	}
} else {
	$response['value'] = 0;
	$response['message'] = "Try Again!";
	echo json_encode($response);
}

