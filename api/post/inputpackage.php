<?php
require_once '../../includes/config_itemsandpackages.php';
require_once '../../includes/config_pictureitemsandpackages.php';
$images = array();
session_start();
if(isset($_POST['submit'])){
    $id_vendor = $_SESSION['id'];
    $option = $_POST['option'];
    $type = $_POST['typesp'];
    $category = $_POST['category'];
    $category_package = implode(', ', $_POST['copackage']);
    $name = $_POST['namesp'];
    $aprice = $_POST['price'];
    $price = str_replace(".", "", $aprice);
    $description = $_POST['description'];
    $stock = $_POST['stock'];


    $filename = $_FILES['picture']['name'];
    $tmpname = $_FILES['picture']['tmp_name'];
    $filetype = $_FILES['picture']['type'];
    $filesize = $_FILES['picture']['size'];

    if ($stock == ""){
        $stock = "-";
        $query = array('id_vendor' => $_SESSION['id'], 'option' => $option, 'type' => $type, 'category' => $category, 'pax' => "No", 'category_package' => $category_package, 'type_duration' => '-', 'name' => $name, 'price' => $price, 'description' => $description, 'status' => 'Available', 'stock' => $stock);
        $collection2 -> insertOne($query);
    } else {
        $query = array('id_vendor' => $_SESSION['id'], 'option' => $option, 'type' => $type, 'category' => $category, 'pax' => "No", 'category_package' => $category_package, 'type_duration' => '-', 'name' => $name, 'price' => $price, 'description' => $description, 'status' => 'Available', 'stock' => $stock);
        $collection2 -> insertOne($query);
    }

    $query2 = array('$and' => array(array('id_vendor' => $id_vendor), array('option' => $option), array('type' => $type), array('category' => $category), array('name' => $name), array('price' => $price), array('description' => $description)));
    $response = $collection2->findOne($query2);

    $id_product2 = $response['_id'];
    $path = "./../../upload/product/";
    $output = $id_vendor.$id_product2;

    for($i=0; $i<=count($tmpname)-1; $i++){
        $a_name = addslashes($filename[$i]);
        $tmp = move_uploaded_file($tmpname[$i], $path.$output.$filename[$i]);
        $path_tmp = $output.$filename[$i];

        $query3 = array('id_product' => $id_product2, 'id_vendor' => $_SESSION['id'], 'name_pic' => $a_name, 'picture' => $path_tmp);
        $collection3 -> insertOne($query3);
    }
    $url = "../../dashboard.php";
    echo ("<script>location.href = '$url'</script>");
}
?>