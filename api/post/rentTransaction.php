<?php

require_once '../../includes/config.php';
require_once '../../includes/config_itemsandpackages.php';

if($_SERVER['REQUEST_METHOD']=='POST'){

	$response = array();

	$id_product = $_POST['id_product'];
	$id_customer = $_POST['id_customer'];
	$id_vendor = $_POST['id_vendor'];
	$name = $_POST['name'];
	$no_hp = $_POST['no_hp'];
	$order = $_POST['order'];
	$option = $_POST['option'];
	$category = $_POST['category'];
	$booking = $_POST['booking'];
	$price = $_POST['price'];
	$type = $_POST['type'];
	$duration = $_POST['duration'];
	$type_duration = $_POST['type_duration'];
	$note = $_POST['note'];
	$paid = $_POST['paid'];
	$minus = $_POST['minus'];
	$total = $_POST['total'];
	$pax = $_POST['pax'];
	$total_pax = $_POST['total_pax'];
	if ($minus == '0'){
		$statusIP = "Available";

		$sql1 = "INSERT INTO `tbl_order` (`id_product`, `id_customer`, `id_vendor`, `name`, `no_hp`, `option`, `type`, `order`, `category`, `date_transaction`, `booking`, `price`, `unit`, `duration`, `type_duration`, `note`, `paid`, `paid_before`, `minus`, `total`, `status`, `pax`, `total_pax`) VALUES ('$id_product', '$id_customer', '$id_vendor', '$name', '$no_hp', '$option', '$type', '$order', '$category', CURRENT_TIMESTAMP, '$booking', '$price', 0, '$duration', '$type_duration', '$note ', '$paid', 0, '$minus', '$total', 'Completed', '$pax', '$total_pax');";
		if(mysqli_query($con, $sql1)){
			$rspn = $collection2->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id_product)),array('$set' => array('status' => $statusIP)));
			$response['kode'] = 1;
			$response['pesan'] = "Input Successful1";
			echo json_encode($response);
		} else {
			$response['kode'] = 0;
			$response['pesan'] = "Input Failed1";
			echo json_encode($response);
		}
	} else {
		$statusIP = "Not Available";

		$sql2 = "INSERT INTO `tbl_order` (`id_product`, `id_customer`, `id_vendor`, `name`, `no_hp`, `option`, `type`, `order`, `category`, `date_transaction`, `booking`, `price`, `unit`, `duration`, `type_duration`, `note`, `paid`, `paid_before`, `minus`, `total`, `status`, `pax`, `total_pax`) VALUES ('$id_product', '$id_customer', '$id_vendor', '$name', '$no_hp', '$option', '$type', '$order', '$category', CURRENT_TIMESTAMP, '$booking', '$price', 0, '$duration', '$type_duration', '$note', '$paid', 0, '$minus', '$total', 'On Payment', '$pax', '$total_pax');";
		if(mysqli_query($con, $sql2)){
			$rspn = $collection2->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id_product)),array('$set' => array('status' => $statusIP)));
			$response['kode'] = 1;
			$response['pesan'] = "Input Successful2";
			echo json_encode($response);
		} else {
			$response['kode'] = 0;
			$response['pesan'] = "Input Failed2";
			echo json_encode($response);
		}
	}
} 