<?php
session_start();

$id = isset($_GET['id']);

if(isset($_POST['submit'])){

	$id = $_POST['id'];
	$email = $_POST['email'];
	$username = $_POST['uname'];

	require_once '../../includes/config_vendor.php';

	$terms = array('$or' => array(array('email' => $email), array('uname' => $username)));
	$response = $collection->findOne($terms);

	if (($response['email'] == null) and ($response['uname'] == null)) {
		$sql2 = $collection->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id)),array('$set' => array('email' => $email, 'uname' => $username)));

		$_SESSION['username'] = $username;
		$_SESSION['email'] = $email;
		echo "<script>window.alert('Update Email/Username Success!')
		window.location='../../dashboard.php'</script>";
	} else {
		echo "<script>window.alert('Email & Username Has Exist !')
		window.location='../../editemailusername.php'</script>";
	} 
}
