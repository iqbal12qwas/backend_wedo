<?php
$responses = array();

if($_SERVER['REQUEST_METHOD']=='POST'){
	
	$id = $_POST['id'];
	$picture = $_POST['picture'];

	require_once'../../includes/config_vendor.php';

	$response = $collection->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id)),array('$set' => array('picture' => $picture)));
	if($response){
		$response['value'] = 1;
		$response['message'] = "Input Success";
		echo json_encode($response);
	} else {
		$response['value'] = 0;
		$response['message'] = "Input Failed";
		echo json_encode($response);
	}
} 