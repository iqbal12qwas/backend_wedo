<?php
require_once '../../includes/config_itemsandpackages.php';
$images = array();

if(isset($_POST['submit'])){
    $id = $_POST['id'];
	$option = $_POST['option'];
    $type = $_POST['typesp'];
    $category = $_POST['category'];
    $category_package = implode(', ', $_POST['copackage']);
    $name = $_POST['namesp'];
    $aprice = $_POST['price'];
    $price = str_replace(".", "", $aprice);
    $description = $_POST['description'];


    $response = $collection2->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id)),array('$set' => array('option' => $option, 'type' => $type, 'category' => $category, 'category_package' => $category_package, 'name' => $name, 'price' => $price, 'description' => $description)));

    $url = "../../dashboard.php";
    echo ("<script>location.href = '$url'</script>");
}
?>