<?php
require_once '../../includes/config_itemsandpackages.php';

if(isset($_POST['submit'])){
  $id = $_POST['id'];
  $option = $_POST['option'];
  $type = $_POST['typesp'];
  $category = $_POST['category'];
  $pax = $_POST['pax'];
  $name = $_POST['namesp'];
  $aprice = $_POST['price'];
  $price = str_replace(".", "", $aprice);
  $description = $_POST['description'];
  $type_duration = $_POST['type_duration'];
  $stock = $_POST['stock'];

  $response = $collection2->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id)),array('$set' => array('option' => $option, 'type' => $type, 'category' => $category, 'pax' => $pax, 'name' => $name, 'price' => $price, 'description' => $description, 'type_duration' => $type_duration, 'stock' => $stock)));

  $url = "../../dashboard.php";
  echo ("<script>location.href = '$url'</script>");
}
?>