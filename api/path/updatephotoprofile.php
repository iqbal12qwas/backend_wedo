<?php


if(isset($_POST['submit'])){
    $id = $_POST['id'];

    $filename = $_FILES['picture']['name'];
    $tmpname = $_FILES['picture']['tmp_name'];
    $filetype = $_FILES['picture']['type'];
    $filesize = $_FILES['picture']['size'];

    $a_name = addslashes($filename);
    $tmp = base64_encode(file_get_contents($tmpname));

    require_once'../../includes/config_vendor.php';

    $response = $collection->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id)),array('$set' => array('picture' => $tmp)));
    echo "<script>window.alert('Update Success')
        window.location='../../dashboard.php'</script>";    
}