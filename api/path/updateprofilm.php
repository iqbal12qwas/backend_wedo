<?php
if($_SERVER['REQUEST_METHOD']=='POST'){
	
	$id = new MongoDB\BSON\ObjectID($_POST['id']);
	$name = $_POST['name'];
	$birthday = $_POST['birthday'];
	$vname = $_POST['vendorname'];
	$no_hp = $_POST['no_hp'];
	$vaddress = $_POST['vaddress'];
	$provinsi = $_POST['provinsi'];
	$responses = array();
	require_once '../../includes/config_vendor.php';

	$responses = $collection->updateOne(array('_id' => $id),array('$set' => array('vname' => $vname, 'no_hp' => $no_hp, 'vaddress' => $vaddress, 'name' => $name, 'provinsi' => $provinsi, 'birthday_date' => $birthday)));

	if($responses){
		$response['value'] = 1;
		$response['message'] = "Input Success";
		echo json_encode($response);
	} else {
		$response['value'] = 0;
		$response['message'] = "Input Failed";
		echo json_encode($response);
	}
}