<?php
session_start();
if(isset($_POST['submit'])){


    $id = $_POST['id'];
    $password1 = $_POST['old_password'];
    $password = md5($password1);
    $confirmpassword = $_POST['confirm_password'];

    require_once '../../includes/config_vendor.php';

    if ($password1 == $confirmpassword) {
        $response = $collection->updateOne(array('_id' => new MongoDB\BSON\ObjectID($id)),array('$set' => array('password' => $password)));
        if ($response){
            $_SESSION['password'] = $password1;
            echo "<script>window.alert('Update Password Success !')
            window.location='../../dashboard.php'</script>";
        }
    } else {
        echo "<script>window.alert('Password & Confirm Password Not Same !')
        window.location='../../editpassword.php?id=$_SESSION[id]&username=$_SESSION[username]'</script>";
    }
}