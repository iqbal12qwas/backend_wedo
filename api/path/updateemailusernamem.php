<?php

$responses = array();

if($_SERVER['REQUEST_METHOD']=='POST'){

	$id = new MongoDB\BSON\ObjectID($_POST['id']);
	$email = $_POST['email'];
	$username = $_POST['uname'];

	require_once '../../includes/config_vendor.php';

	$terms = array('$or' => array(array('email' => $email), array('uname' => $username)));
	$response = $collection->findOne($terms);

	if (($response['email'] == null) and ($response['uname'] == null)) {
		$collection->updateOne(array('_id' => $id),array('$set' => array('email' => $email, 'uname' => $username)));
		$responses['kode'] = 1;
		$responses['pesan'] = "Update Email/Username Success";
		echo json_encode($responses);
	} else {

		$responses['kode'] = 0;
		$responses['pesan'] = "Email/Username Has Exist";
		echo json_encode($responses);		
	} 
}
