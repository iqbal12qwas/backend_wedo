<?php

session_start();
if(!isset($_SESSION['username'])){
    echo "<script>window.alert('You Must Be Log In !')
    window.location='./login.php'</script>";
}

?>
<!DOCTYPE html>
<html>

<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="shortcut icon" href="assets/img/Logo.png"> 
 <title>Add Item</title>
 <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="assets/css/styles.css">

 <style type="text/css">
 input[type=file] {
    display: inline;
}
#image_preview{
    border: 1px solid black;
    padding: 10px;
    margin-top: 10px;
}
#image_preview img{
    border: 1px solid black;
    width: 150px;
    height: 150px;
    padding: 5px;
}
</style>
</head>

<body>
    <?php
    include 'navbar.php';
    ?>
    <div class="container">
        <form action="api/post/inputitem.php" class="form-horizontal" method="post" enctype="multipart/form-data">
            <div class="row">
                <h2 style="text-align: center; margin-bottom: 40px; color: #07E5F4;">INPUT ITEM</h2>
            </div>
            <div class="form-group row">
                <label for="option" class="col-sm-2 col-form-label">Option :</label>
                <div class="col-sm-3">
                    <select class="form-control" name="option">
                        <optgroup label="Option">
                            <option value="-">-</option>
                            <option value="Product">Product</option>
                            <option value="Service">Service</option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="category" class="col-sm-2 col-form-label">Category :</label>
                <div class="col-sm-3">
                    <select class="form-control" name="category" onchange="Check(this);">
                        <optgroup label="Category">
                            <option value="-">-</option>
                            <option value="Wedding Venue">Wedding Venue</option>
                            <option value="Decoration">Decoration</option>
                            <option value="Bridal Fashion">Bridal Fashion</option>
                            <option value="Hair, Makeup, and Wellness">Hair, Makeup, &amp; Wellness</option>
                            <option value="Photographers">Photographers</option>
                            <option value="Videographers">Videographers</option>
                            <option value="Wedding Cakes and Desert">Wedding Cakes &amp; Desert</option>
                            <option value="Flowers and Floral">Flowers &amp; Floral</option>
                            <option value="Invitations">Invitations</option>
                            <option value="Entertainment">Entertainment</option>
                            <option value="Souvenirs">Souvenirs</option>
                            <option value="Catering">Catering</option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group row" id="Yes"  style="display: none;">
                <label for="type" class="col-sm-2 col-form-label">Pax :</label>
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" name="pax" value="No">
                            <input type="checkbox" id="pax" name="pax" value="Yes">(Yes/No)
                        </label>
                        <label style="color: red;"><strong>
                            (If Checked then this Item Price Considered Price/Pax)
                        </strong></label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="type" class="col-sm-2 col-form-label">Type Item :</label>
                <div class="col-sm-3">
                    <select class="form-control" name="typesp" onchange="yesnoCheck(this);">
                        <optgroup label="Type Item">
                            <option value="-">-</option>
                            <option value="Sell">Sell</option>
                            <option value="Rent">Rent</option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group row" id="ifYes" style="display: none;">
                <label for="type_time" class="col-sm-2 col-form-label">Type Time :</label>
                <div class="col-sm-3">
                    <select class="form-control" name="type_duration">
                        <optgroup label="Type Time">
                           <option value="-">-</option>
                           <option value="Days">Days</option>
                           <option value="Hours">Hours</option>
                       </optgroup>
                   </select>
               </div>
           </div>
           <div class="form-group row" id="stock" style="display: none;">
                <label for="stock" class="col-sm-2 col-form-label">Stock :</label>
                <div class="col-sm-3">
                    <input class="form-control" type="text" name="stock" placeholder="Stock Product">
               </div>
           </div>
           <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name :</label>
            <div class="col-sm-3">
                <input class="form-control" type="text" name="namesp" maxlength="20" placeholder="Name Service/Product" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label">Price :</label>
            <div class="col-sm-3">
                <input class="form-control" type="text" name="price" id="rupiah" placeholder="Price" inputmode="numeric"  onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" required>
            </div>
            <div class="col-sm-1" style="display: none;" id="show">
                <h5 style="color: red;"><strong>
                    / Pax
                </strong></h5>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description :</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="5" id="description" name="description" placeholder="Description Service/Product" required></textarea>
            </div>
        </div>
        <h6 style="color: #FD1C1C;"><strong>*Upload Your Images/Pictures</strong></h6>
        <input class="form-control" type="file" multiple accept=".png,.jpg,.jpeg,.gif" id="picture" name="picture[]"  required>
        <div id="image_preview" style="margin-bottom: 10px;"></div>
        <div class="col-md-1 col-md-offset-1 col-md-push-5">
            <input class="btn btn-info" type="submit" name="submit" value="Finish">
        </div>
    </form>
</div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="css/additem.js"></script>
</body>

<script type="text/javascript">

    $('#picture').change(function(){
        $('#image_preview').html("");
        var total_file = document.getElementById("picture").files.length;

        for(var i=0; i<total_file;i++) {
            $('#image_preview').append("<img src = '"+URL.createObjectURL(event.target.files[i])+"'>");
        }
    });

    $('form').ajaxForm(function() {
        alert("Uploaded SuccessFully");
    })
</script>

<script type="text/javascript">

    function yesnoCheck(that) {
        if (that.value == "Rent") {
          document.getElementById("ifYes").style.display = "block";
          document.getElementById("stock").style.display = "none";
      } else {
          document.getElementById("ifYes").style.display = "none";
          document.getElementById("stock").style.display = "block";
      }

       if (that.value == "Sell") {
          document.getElementById("stock").style.display = "block";
          document.getElementById("ifYes").style.display = "none";
      } else {
          document.getElementById("stock").style.display = "none";
          document.getElementById("ifYes").style.display = "block";
      }

      if (that.value == "-") {
          document.getElementById("stock").style.display = "none";
          document.getElementById("ifYes").style.display = "none";
      } 

  }


  function Check(that) {
      if ((that.value == "Wedding Venue") || (that.value == "Catering") || (that.value == "Souvenirs") || (that.value == "Invitations")) {
          document.getElementById("Yes").style.display = "block";
      } else {
          document.getElementById("Yes").style.display = "none";
      }
  }

  $(function () {
        $("#pax").click(function () {
            if ($(this).is(":checked")) {
                $("#show").show();
            } else {
                $("#show").hide();
            }
        });
    });
</script>
</html>

