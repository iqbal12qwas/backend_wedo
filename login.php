<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/Logo.png"> 
    <title>Log In</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/login.css">
    <link rel="stylesheet" href="assets/css/register.css">
</head>

<body>
    <form action="api/post/inputlogin.php" method="post">
        <div class="container">
            <div class="row">
                <div><img class="img-responsive" src="assets/img/Logo.png" style="margin-top: 50px;" width="100" height="100">
                    <h1>Vendor Log In Panel</h1>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-md-offset-5"><a class="btn btn-info" role="button" href="register.php" style="margin-left: 40px;">Register</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-push-4">
                    <p class="text-center">Email/Username</p>
                </div>
                <div class="col-lg-2 col-md-push-4">
                    <input class="form-control input-sm" type="text" name="uname" required="">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-push-4">
                    <p class="text-center">Password</p>
                </div>
                <div class="col-lg-2 col-md-push-4">
                    <input class="form-control input-sm" type="password" id="password" name="password" required="">
                    <input type="checkbox" onclick="myFunction1()"> Show Password
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-1 col-md-offset-5">
                    <button class="btn btn-info" type="submit" name="submit" style="margin-left: 30px;">Log In Now</button>
                </div>
            </div>
        </div>
    </div>
</form>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
    function myFunction1() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
</body>

</html>







