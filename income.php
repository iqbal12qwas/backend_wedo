    <?php
    include "includes/config.php";

    session_start();
    if(!isset($_SESSION['username'])){
        echo "<script>window.alert('You Must Be Log In !')
        window.location='./login.php'</script>";
    }

    $ses = $_SESSION['username'];

    ?>

    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="assets/img/Logo.png"> 
        <title>Income</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <link rel="stylesheet" href="css/dashboard.css">
        <link rel="stylesheet" href="css/income.css">


    </head>

    

<body>
   <div class="container-full" >
    <nav class="navbar navbar-expand-sm bg-info navbar-primary">
        <div class="container-fluid" >
            <div class="navbar-header"><a class="navbar-brand navbar-link" href="dashboard.php"><i class="fa fa-fw fa-home"></i>WedO </a>
               <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><i class="fa fa-bars" style="color: #44929b;"></i></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav mr-auto">
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fa fa-list"></i> Category<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li role="presentation"><a href="category/weddingvenue.php"><i class="fa fa-building"></i> Wedding Venue</a></li>
                            <li role="presentation"><a href="category/decoration.php"><i class="fa fa-gavel"></i> Decoration</a></li>
                            <li role="presentation"><a href="category/bridalfashion.php"><i class="fa fa-female"></i> Bridal Fashion</a></li>
                            <li role="presentation"><a href="category/hair_makeup_wellness.php"><i class="fa fa-paint-brush"></i> Hair, Makeup, & Wellness</a></li>
                            <li role="presentation"><a href="category/photographers.php"><i class="fa fa-camera-retro"></i> Photographers</a></li>
                            <li role="presentation"><a href="category/videographers.php"><i class="fa fa-video-camera"></i> Videographers</a></li>
                            <li role="presentation"><a href="category/wedding_cakes_desert"><i class="fa fa-birthday-cake"></i> Wedding Cakes & Desert</a></li>
                            <li role="presentation"><a href="category/flowers_floral.php"><i class="fa fa-pagelines"></i> Flowers & Floral</a></li>
                            <li role="presentation"><a href="category/invitations.php"><i class="fa fa-id-card-o"></i> Invitations</a></li>
                            <li role="presentation"><a href="category/entertaiment.php"><i class="fa fa-slideshare"></i> Entertaiment</a></li>
                            <li role="presentation"><a href="category/souvenirs.php"><i class="fa fa-shopping-bag"></i> Souvenirs</a></li>
                            <li role="presentation"><a href="category/catering.php"><i class="fa fa-cutlery"></i> Catering</a></li>
                            <li role="presentation"><a href="category/package.php"><i class="fa fa-gift"></i> Package</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fa fa-plus-square-o"></i> Add<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li role="presentation"><a href="additem.php"><i class="fa fa-briefcase"></i> Item</a></li>
                            <li role="presentation"><a href="addpackage.php"><i class="fa fa-th"></i> Package</a></li>
                        </ul>
                    </li>
                    <li role="presentation"><a href="income.php"><i class="fa fa-table"></i> Income </a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fa fa-user-circle"></i> <?php
                    echo $ses; ?><span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><?php echo "<a href='editprofile.php?id=$_SESSION[id]&username=$_SESSION[username]'><i class='fa fa-user'></i> Edit Profile </a>" ?></li>
                        <li role="presentation"><?php echo "<a href='editemailusername.php?id=$_SESSION[id]&username=$_SESSION[username]'><i class='fa fa-at'></i> Edit Email/Username </a>" ?></li>
                        <li role="presentation"><?php echo "<a href='editpassword.php?id=$_SESSION[id]&username=$_SESSION[username]'><i class='fa fa-key'></i> Edit Password </a>" ?></li>
                        <li role="presentation"><a href="api/post/logout.php"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
</div>
<div class="container-full">
    <form method="post" action="export.php">
        <div class="input-append date">
            <span style="margin-right: 10px;"><strong>Booking Start : </strong><input type="date" name="date_start" required></span>
            <span style="margin-right: 10px;"><strong>Booking End : </strong><input type="date" name="date_end" required></span>
            <input type="submit" name="export" value="Print Excel Data Income" class="btn btn-info" src="assets/img/supported-platforms-excel-logo-png-3.png" style="margin-bottom: 10px;  ">
        </div>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>No Hp</th>
                    <th>Type</th>
                    <th>Order</th>
                    <th>Category</th>
                    <th>D.Acquittance</th>
                    <th>D.Booking</th>
                    <th>Price(Rp)</th>
                    <th>Unit</th>
                    <th>Duration</th>
                    <th>T.Time</th>
                    <th>Note</th>
                    <th>Paid(Rp)</th>
                    <th>Total(Rp)</th>
                    <th>Keterangan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                //Pagination
                $halperpage = 10;

                $page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;

                $mulai = ($page>1) ? ($page * $halperpage) - $halperpage : 0;

                $result =mysqli_query($con, "SELECT * FROM `tbl_data_revenue` WHERE `id_vendor` = '$_SESSION[id]'");

                $total = mysqli_num_rows($result);

                $pages = ceil($total/$halperpage);            

                $qry = mysqli_query($con, "SELECT * FROM `tbl_data_revenue` WHERE `id_vendor` = '$_SESSION[id]' ORDER BY `booking` DESC  LIMIT $mulai, $halperpage")or(mysqli_error);

                $no = $mulai+1;

                $no = 1;
                while($req = mysqli_fetch_assoc($qry)){
                    $note = str_replace('<br />', "\n", $req['note']);
                    ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= substr($req['name'], 0, 15) ?></td>
                        <td><?= $req['no_hp'] ?></td>
                        <td><?= $req['type'] ?></td>
                        <td><?= substr($req['order'], 0, 15) ?></td>
                        <td><?= substr($req['category'], 0, 15) ?></td>
                        <td><?= $req['date_done'] ?></td>
                        <td><?= $req['booking'] ?></td>
                        <td><?= number_format($req['price'], 0, ".", ".") ?></td>
                        <td><?= $req['unit'] ?></td>
                        <td><?= $req['duration'] ?></td>
                        <td><?= $req['type_duration'] ?></td>
                        <td><?= substr($req['note'], 0, 10)?></td>
                        <td><?= number_format($req['paid'], 0, ".", ".") ?></td>
                        <td><?= number_format($req['total'], 0, ".", ".") ?></td>
                        <td><?php echo" <a href='#myModal' class='btn btn-info btn-xs edit_data' id='custId' data-toggle='modal' data-id=".$req['id'].">Edit</a>
                        <a href='api/delete/deleteincome.php?id=$req[id]' class='btn btn-danger btn-xs delete_data' onclick=\"return  confirm('Delete This Data ?')\">Delete</a>"?></td>
                    </tr>
                    <?php
                } 
                ?>
            </tbody>
        </table>
        <div class="container">
            <?php for ($i=1; $i<=$pages ; $i++){ ?>
                <span style="margin-right: 10px;"><strong>Page : </strong><a class="btn btn-info btn-md" href="?halaman=<?php echo $i; ?>"><?php echo $i; ?></a></span>
            <?php } ?>
        </div> 
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Data Income</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="fetched-data"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

<script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            $.ajax({
                type : 'post',
                url : 'editincome.php',
                data :  'rowid='+ rowid,
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });
    });
</script>



</html>