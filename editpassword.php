<?php
include "includes/config.php";

session_start();
if(!isset($_SESSION['username'])){
    echo "<script>window.alert('You Must Be Log In !')
    window.location='./login.php'</script>";
}


?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/Logo.png"> 
    <title>Edit Password</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="css/editprofil.css">
</head>

<body>
   <?php
   include 'navbar.php';
   ?>
   <div class="container">
     <div class="row">
        <div class="text-h1">
            <p class="text-p">Edit Password</p>
        </div>
    </div>
    <form action="api/path/updatepassword.php" method="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-push-4">
                    <input class="form-control" type="text" required="" name="id" value="<?php echo $_SESSION['id'];?>" style="display: none;">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-push-4">
                    <p class="text-left">Password </p>
                </div>
                <div class="col-md-3 col-md-push-4">
                    <input class="form-control input-field" type="password" required="" name="old_password" id="password1" onkeyup="check();" value="<?php echo $_SESSION['password'];?>">
                    <input type="checkbox" onclick="myFunction1()"> Show Password
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-push-4">
                    <p class="text-left">Confirm Password </p>
                </div>
                <div class="col-md-3 col-md-push-4">
                    <input class="form-control" type="password" required="" name="confirm_password" id="confirm_password" onkeyup="check();">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-1 col-md-push-4">
                    <span id="message"></span>
                </div>
            </div>
        </div>
        <div class="container" style="margin-top: 30px">
            <div class="row">
                <div class="col-md-1 col-md-offset-1 col-md-push-4">
                    <button class="btn btn-info btn-lg" type="submit" name="submit" id="update">Update</button>
                </div>
            </div>
        </div>
    </form>
</div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

<script type="text/javascript">
    var check = function() {
      if (document.getElementById('password1').value == document.getElementById('confirm_password').value) {
        document.getElementById('message').style.color = 'green';
        document.getElementById('message').innerHTML = 'Matching';
    } else {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'Not Matching';
    }
}

function myFunction1() {
    var x = document.getElementById("password1");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

</script>

</html>

