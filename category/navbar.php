<?php
$ses = $_SESSION['username'];
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WebsiteWedO</title>
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/styles.css">
    <link rel="stylesheet" href="../css/dashboard.css">
</head>

<body>
    <div class="container-full" >
        <nav class="navbar navbar-expand-sm bg-info navbar-primary">
            <div class="container-fluid" >
                <div class="navbar-header"><a class="navbar-brand navbar-link" href="../dashboard.php"><i class="fa fa-fw fa-home"></i>WedO </a>
                    <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><i class="fa fa-bars" style="color: #44929b;"></i></button>
                </div>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fa fa-list"></i> Category<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="weddingvenue.php"><i class="fa fa-building"></i> Wedding Venue</a></li>
                                 <li role="presentation"><a href="decoration.php"><i class="fa fa-gavel"></i> Decoration</a></li>
                                <li role="presentation"><a href="bridalfashion.php"><i class="fa fa-female"></i> Bridal Fashion</a></li>
                                <li role="presentation"><a href="hair_makeup_wellness.php"><i class="fa fa-paint-brush"></i> Hair, Makeup, &     Wellness</a></li>
                                <li role="presentation"><a href="photographers.php"><i class="fa fa-camera-retro"></i> Photographers</a></li>
                                <li role="presentation"><a href="videographers.php"><i class="fa fa-video-camera"></i> Videographers</a></li>
                                 <li role="presentation"><a href="wedding_cakes_desert.php"><i class="fa fa-birthday-cake"></i> Wedding Cakes & Desert</a></li>
                                 <li role="presentation"><a href="flowers_floral.php"><i class="fa fa-pagelines"></i> Flowers & Floral</a></li>
                                 <li role="presentation"><a href="invitations.php"><i class="fa fa-id-card-o"></i> Invitations</a></li>
                                 <li role="presentation"><a href="entertainment.php"><i class="fa fa-slideshare"></i> Entertainment</a></li>
                                 <li role="presentation"><a href="souvenirs.php"><i class="fa fa-shopping-bag"></i> Souvenirs</a></li>
                                <li role="presentation"><a href="catering.php"><i class="fa fa-cutlery"></i> Catering</a></li>
                                <li role="presentation"><a href="package.php"><i class="fa fa-gift"></i> Package</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fa fa-plus-square-o"></i> Add<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="../additem.php"><i class="fa fa-briefcase"></i> Item</a></li>
                                <li role="presentation"><a href="../addpackage.php"><i class="fa fa-th"></i> Package</a></li>
                            </ul>
                        </li>
                        <li role="presentation"><a href="../income.php"><i class="fa fa-table"></i> Income </a></li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fa fa-user-circle"></i> <?php
                        echo $ses; ?><span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><?php echo "<a href='../editprofile.php?id=$_SESSION[id]&username=$_SESSION[username]'><i class='fa fa-user'></i> Edit Profile </a>" ?></li>
                                 <li role="presentation"><?php echo "<a href='../editemailusername.php?id=$_SESSION[id]&username=$_SESSION[username]'><i class='fa fa-at'></i> Edit Email/Username </a>" ?></li>
                                <li role="presentation"><?php echo "<a href='../editpassword.php?id=$_SESSION[id]&username=$_SESSION[username]'><i class='fa fa-key'></i> Edit Password </a>" ?></li>
                                <li role="presentation"><a href="../api/post/logout.php"><i class="fa fa-sign-out"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>